<?php

/**
* 
*/
class Request_model extends CI_Model
{
	
	public function __construct()
	{
		# code...
		$this->load->database();
	}

	public function get_request(){

		$this->db->select('*');
		$this->db->from('request');
		$query = $this->db->get();
		$result = $query->result_array();
		$customer = array();
		foreach ($query->result_array() as $key => $row) {

			// echo $row."asdasd";
			# code...
			
			$arr_customer = $this->request_model->get_client($row['id_customer']);
			$result[$key]['nama_customer'] = $arr_customer[0]['nama_customer'];
			$result[$key]['email'] = $arr_customer[0]['email'];

		}
		
		return $result;

	}

	public function get_barang($id_barang){

		$this->db->select('*');
		$this->db->from('barang');
		$this->db->where('id_barang', $id_barang);
		$query2 = $this->db->get();
		$result = $query2->result_array();

		return $result;

	}

	public function get_client($id_customer){

		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where('id_customer', $id_customer);
		$query2 = $this->db->get();
		$result = $query2->result_array();

		return $result;

	}

	public function request_detail($request){

		$this->db->select('*');
		$this->db->from('request');
		$this->db->where('kode_offer', $request);
		$query = $this->db->get();
		$data_array = $query->result_array();

		$result = array();
		$result[0]['kode_offer'] = $data_array[0]['kode_offer'];
		$result[0]['tanggal'] = $data_array[0]['tanggal'];
		$result[0]['pesan'] = $data_array[0]['pesan'];
		foreach ($query->result() as $row) {

			$this->db->select('*');
			$this->db->from('customer');
			$this->db->where('id_customer', $row->id_customer);
			$query2 = $this->db->get();


			foreach ($query2->result() as $row2) {

				$result[0]['id_customer'] = $row2->id_customer;
				$result[0]['nama_customer'] = $row2->nama_customer;
				$result[0]['email'] = $row2->email;

			}

			$arrBarang = explode(",", $row->barang);
			foreach ($arrBarang as $key => $value) {

				$this->db->select('*');
				$this->db->from('barang');
				$this->db->where('id_barang', $value);
				$query3 = $this->db->get();
				$result[0]['barang'][$key] = $query3->result_array();

				// foreach ($query3->result() as $row3) {

				// 	$result[0]['barang'][$key]['type_barang'] = $row3->Type;
				// 	$result[0]['barang'][$key]['nama_barang'] = $row3->nama_barang;
				// 	$result[0]['barang'][$key]['id_barang'] = $row3->id_barang;

				// }

			}

		}
		return $result; 

	}

	public function insert_penawaran($data){

		$this->db->insert('penawaran', $data);
		return true;
		
	}


}


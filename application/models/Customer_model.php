<?php

/**
* Customer Model
*/
class Customer_model extends CI_Model
{

	public function __construct()
	{
		# code...
		$this->load->database();
	}

	public function get_customer($id=""){

		$this->db->select('*');
		$this->db->from('customer');
		if ($id) {
			$this->db->where('id_customer', $id);
		}
		$query = $this->db->get();
		
		return $query->result_array();

	}

}
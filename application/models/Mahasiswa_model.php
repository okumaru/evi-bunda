<?php

/**
* 
*/
class Mahasiswa_model extends CI_Model
{
	
	public function __construct()
	{
		# code...
		$this->load->database();
	}

	public function get_mhs(){

		$this->db->select('*');
		$this->db->from('mahasiswa');
		$query = $this->db->get();
		
		return $query->result_array();

	}

	public function store_mhs($data){

		$insert = $this->db->insert('mahasiswa', $data);
	    return $insert;

	}

	public function get_mahasiswa_by_id($nim){

		$this->db->select('*');
		$this->db->from('mahasiswa');
		$this->db->where('nim', $nim);
		$query = $this->db->get();
		return $query->result_array(); 

	}

	public function update_mahasiswa($nim, $data){

		$this->db->where('nim', $nim);
		$this->db->update('mahasiswa', $data);
		return true;

	}

	public function delete_mahasiswa($nim){

		$this->db->where('nim',$nim);
		$this->db->delete('mahasiswa');

	}

	public function sudah_memilih($nim){

		$this->db->select('status');
		$this->db->from('mahasiswa');
		$this->db->where('nim', $nim);
		$query = $this->db->get();
		return $query->result_array(); 

	}

	public function insert_memilih($data){

		$insert = $this->db->insert('vote', $data);
	    return $insert;

	}

}
<?php

/**
* client model
*/
class Client_model extends CI_Model
{
	
	public function __construct()
	{
		# code...
		$this->load->database();
	}

	public function get_barang($id=""){

		$this->db->select('*');
		$this->db->from('barang');
		if ($id) {
			$this->db->where('id_barang', $id);
		}
		$query = $this->db->get();
		
		return $query->result_array();

	}

	public function get_customer($email){

		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where('email', $email);
		$query = $this->db->get();
		$result = $query->result_array();

		return $result;

	}

	public function id_customer($id_customer){

		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where('id_customer', $id_customer);
		$query = $this->db->get();
		$result = $query->result_array();

		return $result;

	}

	public function insert_penawaran($data){
		
		$first_result_customer = $this->client_model->get_customer($data['customer']['email']);
		$count_result = count($first_result_customer);
		if ($count_result == 0) {
			$this->db->insert('customer', $data['customer']);
		}

		$second_result_customer = $this->client_model->get_customer($data['customer']['email']);
		$data_request = array(
			'tanggal' => date('Y-m-d'),
			'id_customer' => $second_result_customer[0]['id_customer'],
			'barang' => $data['request']['id_barang'],
			'pesan' => $data['request']['pesan'],
		);
		$this->db->insert('request', $data_request);
	    return 1;

	}

	public function get_penawaran($no_penawaran){

		$this->db->select('*');
		$this->db->from('penawaran');
		$this->db->where('no_penawaran', $no_penawaran);
		$query = $this->db->get();
		
		return $query->result_array();

	}

	public function get_second_penawaran($id_request){

		$this->db->select('*');
		$this->db->from('penawaran');
		$this->db->where('id_request', $id_request);
		$query = $this->db->get();
		
		return $query->result_array();

	}

	public function insert_purchase($data_purchase){

		$this->db->insert('purchase', $data_purchase);
		return 1;

	}

	public function get_purchase($no_penawaran){

		$this->db->select('*');
		$this->db->from('purchase');
		$this->db->where('no_penawaran',$no_penawaran);
		$query = $this->db->get();

		return $query->result_array();

	}

}
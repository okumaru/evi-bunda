<?php

/**
* Kandidat_model.php
*/
class Kandidat_model extends CI_Model
{
	
	public function __construct()
	{
		# code...
		$this->load->database();
	}

	public function get_kandidat(){

		$this->db->select('*');
		$this->db->from('kandidat');
		$query = $this->db->get();
		
		return $query->result_array();

	}

	public function count_polling($id){

		$this->db->select('*');
		$this->db->from('vote');
		$this->db->where('id_kandidat',$id);
		$query = $this->db->get();
		$rowcount = $query->num_rows();
		
		return $rowcount;

	}

	public function detail_polling($id){

		$this->db->select('nim');
		$this->db->from('vote');
		$this->db->where('id_kandidat', $id);
		$query = $this->db->get();

		return $query->result_array();

	}

	public function store_kandidat($data){

		$insert = $this->db->insert('kandidat', $data);
	    return $insert;

	}

	public function delete_kandidat($id){

		$this->db->where('id',$id);
		$this->db->delete('kandidat');

	}

	public function get_kandidat_by_id($id){

		$this->db->select('*');
		$this->db->from('kandidat');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->result_array(); 

	}

	public function update_kandidat($id, $data){

		$this->db->where('id', $id);
		$this->db->update('kandidat', $data);
		return true;

	}

}
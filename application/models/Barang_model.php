<?php

/**
* Barang Model
*/
class Barang_model extends CI_Model
{
	
	public function __construct()
	{
		# code...
		$this->load->database();
	}

	public function get_barang($id=""){

		$this->db->select('*');
		$this->db->from('barang');
		if ($id) {
			$this->db->where('id_barang', $id);
		}
		$query = $this->db->get();
		
		return $query->result_array();

	}

	public function update_barang($id, $data){

		$this->db->where('id_barang', $id);
		$this->db->update('barang', $data);
		return true;

	}

	public function add_barang($data){

		$this->db->insert('barang', $data);
		return true;

	}

	public function delete_barang($id){

		$this->db->where('id_barang',$id);
		$this->db->delete('barang');

	}

}
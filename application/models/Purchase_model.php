<?php

/**
* Purchasing Order Model
*/
class Purchase_model extends CI_Model
{
	
	public function __construct()
	{
		# code...
		$this->load->database();
	}

	public function get_purchase($id=""){

		$this->db->select('*');
		$this->db->from('purchase');
		if ($id) {
			$this->db->where('id', $id);
		}
		$query = $this->db->get();
		$result = array();
		foreach ($query->result_array() as $key => $value) {

			$result[$key]['id'] = $value['id'];
			$result[$key]['no_penawaran'] = $value['no_penawaran'];
			$result[$key]['barang_qty'] = $value['barang_qty'];
			$result[$key]['tanggal'] = $value['tanggal'];

			$arr_penawaran = $this->purchase_model->get_penawaran($value['no_penawaran']);
			$id_customer = $arr_penawaran[0]['id_customer'];
			$arr_customer = $this->purchase_model->get_customer($id_customer);

			$result[$key]['nama_customer'] = $arr_customer[0]['nama_customer'];
			$result[$key]['id_customer'] = $arr_customer[0]['id_customer'];
			

		}
		
		return $result;

	}

	public function get_penawaran($id=""){

		$this->db->select('*');
		$this->db->from('penawaran');
		if ($id) {
			$this->db->where('id_request',$id);
		}
		$query = $this->db->get();
		$result = array();
		foreach ($query->result_array() as $key => $value) {
			$result[$key]['no_penawaran'] = $value['no_penawaran'];
            $result[$key]['tanggal'] = $value['tanggal'];
            $result[$key]['id_customer'] = $value['id_customer'];
            $result[$key]['id_request'] = $value['id_request'];
            $result[$key]['barang'] = $value['barang'];

            $arr_customer = $this->purchase_model->get_customer($value['id_customer']);
			$result[$key]['nama_customer'] = $arr_customer[0]['nama_customer'];

		}
		return $result;

	}

	public function insert_purchase($data){

		if ($this->db->insert('purchase', $data)) {
			return TRUE;
		}else{
			return FALSE;
		}

	}

	public function get_customer($id){

		$this->db->select('*');
		$this->db->from('customer');
		$this->db->where('id_customer', $id);
		$query2 = $this->db->get();
		$result = $query2->result_array();

		return $result;

	}

	public function purchase_detail_barang($id){

		$this->db->select('*');
		$this->db->from('barang');
		$this->db->where('id_barang', $id);
		$query = $this->db->get();
		
		return $query->result_array();

	}

}
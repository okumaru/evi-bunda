<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'client/index';
$route['index'] = 'client/index';
$route['detail'] = 'client/detail';
$route['detail/(:any)'] = 'client/detail';

$route['cart'] = 'client/cart';
$route['penawaran'] = 'client/penawaran';
$route['purchase'] = 'client/purchase';
$route['purchase/(:any)'] = 'client/purchase';

$route['add_product'] = 'client/add_product';
$route['add_product/(:any)'] = 'client/add_product';

$route['remove_product'] = 'client/remove_product';
$route['remove_product/(:any)'] = 'client/remove_product';

$route['admin'] = 'user/index';
$route['admin/login'] = 'user/index';
$route['admin/logout'] = 'user/logout';
$route['admin/login/validate_credentials'] = 'user/validate_credentials';

$route['admin/customer'] = 'customer/index';
$route['admin/customer/detail/'] = 'customer/detail';
$route['admin/customer/detail/(:any)'] = 'customer/detail';

$route['admin/po'] = 'purchase/index';
$route['admin/po/add'] = 'purchase/add';
#$route['admin/po/add/(:any)'] = 'purchase/add';
$route['admin/po/detail/'] = 'purchase/detail';
$route['admin/po/detail/(:any)'] = 'purchase/detail';

$route['admin/barang'] = 'barang/index';
$route['admin/barang/add'] = 'barang/add';
$route['admin/barang/update/'] = 'barang/update';
$route['admin/barang/update/(:any)'] = 'barang/update';
$route['admin/barang/delete/(:any)'] = 'barang/delete';

$route['admin/request'] = 'request/index';
$route['admin/request/detail/'] = 'request/detail';
$route['admin/request/detail/(:any)'] = 'request/detail';
$route['admin/request/penawaran/'] = 'request/penawaran';
$route['admin/request/penawaran/(:any)'] = 'request/penawaran';
$route['admin/request/view_penawaran/'] = 'request/view_penawaran';

$route['admin/penawaran'] = 'purchase/list_penawaran';
$route['admin/penawaran/detail/'] = 'purchase/detail_penawaran';
$route['admin/penawaran/detail/(:any)'] = 'purchase/detail_penawaran';

#$route['404_override'] = '';
#$route['translate_uri_dashes'] = FALSE;
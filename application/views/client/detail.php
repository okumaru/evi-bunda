<div class="wrap-content-second clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/client/product.css" />
                	<script src="<?php echo base_url(); ?>assets/js/client/jssocials.min.js"></script>
				    <link href="<?php echo base_url(); ?>assets/css/client/style-jssocial.min.css" rel="stylesheet" />
				    <script src="<?php echo base_url(); ?>assets/js/client/react.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/react-dom.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/react-intl.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/browser.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/browser.js"></script>

					<div class="row">
					    <div class="col-xs-12">
					        <ol class="breadcrumb fm-breadcrumb jm-breadcrumb">
					            <li>
					            	<a href="/">
					            		<i class="material-icons md-18">home</i>
					            	</a>
					            </li>
					            <li id="liProd" class="active" itemscope="" >
					                <a href="javascript:;" class="current">
					                    <span itemprop="title"><?php echo $barang[0]['nama_barang'];?></span>
					                </a>
					            </li>
					        </ol>
					    </div>
					</div>

					<div class="inner-content">
				        <div id="productDetail1">
				            <div class="row">
				                <div class="col-xs-12 col-sm-4">
				                    <div id="" class="image-container main-image">
				                        <a href="javascript:;">
				                            <img 
				                            	src="<?php echo base_url()."uploads/".$barang[0]['image']; ?>" 
				                            	id="imageMain" 
				                            	class="imgLoader img-responsive" 
				                            	itemprop="image" 
				                            	alt="<?php echo $barang[0]['nama_barang'];?>" 
				                            />
				                        </a>
				                    </div>
			                    </div>

			                    <div class="col-xs-12 col-sm-8">
				                    <h1 class="title-single-product fm-title-single-product">
				                    	<?php echo $barang[0]['nama_barang'];?>
				                	</h1>
				                </div>

				                <div class="col-xs-12 col-sm-8">
				                    <div class="row">
				                        <div class="col-sm-12 col-xs-12 col-md-8">
				                            <div class="form-horizontal info-product-page">
				                                <div id="cphMasterContent_cphContent_divCountryOrigin" class="form-group">
				                                    <label class="col-xs-6 col-sm-6 col-md-6 control-label">Negara Asal</label>
				                                    <label class="col-xs-6 col-sm-6 col-md-6 control-label">Indonesia</label>
				                                </div>
				                                
				                                <div class="form-group price-group">
				                                    <label class="col-xs-6 col-sm-6 col-md-6 control-label">
				                                        Harga
				                                        <br>
				                                        <span class="price-detail-product">
				                                        IDR <?php echo $barang[0]['harga'];?>
				                                    	</span>
				                                    </label>
				                                    
				                                </div>
				                            </div>
				                        </div>
				                        <div class="col-xs-12 np-lr hidden-sm hidden-md hidden-lg">
				                            <hr class="breakline--xs">
				                        </div>
				                        <div class="col-sm-12 col-xs-12 col-md-4">
				                        	<label class="title-need-rfq">Butuh Penawaran Harga, bantuan atau Konsultasi Produk?</label>
				                            <div id="cphMasterContent_cphContent_btnkliknoHp" class="klik-number">
				                                
				                                <div class="hidden2">
				                                    <a href='tel:+6281333202221'>(+62) 81333202221</a>
				                                    <br />
				                                    <a href='tel:+62341794939'>(+62) 341794939</a>
				                                    <br />
				                                    <a href='tel:+62341794385'>(+62) 341794385</a>
				                                </div>
				                            </div>
				                            <div class="starting-applozic-button">
				                                <div id="cphMasterContent_cphContent_DivChatonlineuser">
				                                    <div class="mck-ol-status mck-user-ol-status n-vis" style="width: 100%;" data-mck-id="80863">
				                                    	<a href="<?php echo site_url("add_product/").$barang[0]['id_barang']?>">
				                                    		<button class="btn btn-outline btn-block popup-chat applozic-launcher" data-mck-id='80863'>
					                                            Add To Cart
					                                        </button>
				                                    	</a>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
					<div class="row">
				        <hr class="breakline--xs">
				    </div>

				    <div class="desc-content tab-detail-product">
				        <div class="row">
				            <div class="col-md-8 col-lg-9">
				                <ul class="nav nav-tabs">
				                    <li class="active"><a href="#home" data-toggle="tab">Spesifikasi</a></li>
				                </ul>
				                <div class="tab-content">
				                    <div role="tabpanel" class="tab-pane active" id="home">
				                    	<h2>Spesifikasi  <?php echo $barang[0]['nama_barang'];?></h2>
				                        <p>
				                        	<?php echo $barang[0]['Spesifikasi'];?>
				                        </p>
									</div>
								</div>
							</div>
						</div>
						
                    </div>
                </div>
            </div>
		    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/product.js"></script>
		    <script type="text/javascript">
		    	<?php 
		    		if ($added) {
		    			echo 'alert("Barang Sudah di Tambahkan");';
		    			echo 'window.location.href = "'.site_url("detail/").$barang[0]['id_barang'].'"';
		    		}
		    	?>
		    </script>

		</div>
</div>
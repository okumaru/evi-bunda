<div class="wrap-content-second clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/client/product.css" />
                	<script src="<?php echo base_url(); ?>assets/js/client/jssocials.min.js"></script>
				    <link href="<?php echo base_url(); ?>assets/css/client/style-jssocial.min.css" rel="stylesheet" />
				    <script src="<?php echo base_url(); ?>assets/js/client/react.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/react-dom.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/react-intl.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/browser.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/browser.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/jquery.dataTables.bootstrap.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/jquery.dataTables.min.js"></script>

                	<div class="row">
					    <div class="col-xs-12">
					        <ol class="breadcrumb fm-breadcrumb jm-breadcrumb">
					            <li>
					            	<a href="/">
					            		<i class="material-icons md-18">home</i>
					            	</a>
					            </li>
					            <li id="liProd" class="active" itemscope="" >
					                <a href="javascript:;" class="current">
					                    <span itemprop="title">Purchase</span>
					                </a>
					            </li>
					        </ol>
					    </div>
					</div>
					<div class="inner-content">
				        <div id="productDetail1">
				            <div class="row">
				            	<?php

				            	if ($step == 0) {
				            		
				            			if($this->session->flashdata('flash_message')){
									        if($this->session->flashdata('flash_message') == 'updated'){
									        	echo '<div class="alert alert-danger">';
									            echo '<a class="close" data-dismiss="alert">×</a>';
									            echo '<strong>Error!</strong> Penawaran sudah digunakan .<br/>';
									            echo '<a href="'.base_url().'">Close</a>';
									          	echo '</div>';       

									        }elseif($this->session->flashdata('flash_message') == 'empty'){
									        	echo '<div class="alert alert-danger">';
									            echo '<a class="close" data-dismiss="alert">×</a>';
									            echo '<strong>Error!</strong> Penawaran tidak ada .<br/>';
									            echo '<a href="'.base_url().'">Close</a>';
									          	echo '</div>';       
									        }
									    }
				            		$attributes = array('class' => 'form-horizontal', 'id' => '');
					            	echo form_open_multipart('purchase', $attributes);

					            	?>
					            	<div class="form-horizontal control-group">
					            		<input type="hidden" name="one" value="one">
						            	<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 
												No. Penawaran
											</label>
											<div class="col-sm-9 controls">
												<input type="text" id="form-field-1" name="no_penawaran" 
												placeholder="No. Penawaran" class="col-xs-10 col-sm-5" />
											</div>
										</div>

						            </div>
						            <button class="btn btn-outline btn-block popup-chat applozic-launcher" 
						              	style="background-color: #15C0C7; color: white;" 
						               	data-mck-id="80863">
										Check Penawaran
							        </button>
					            	<?php

					            	echo form_close();

				            	}elseif ($step == 1) {
				            		if ($barang) {
				            			// echo "<pre>";
				            			// print_r($barang);
				            			// echo "</pre>";

				            			if($this->session->flashdata('flash_message')){
									        if($this->session->flashdata('flash_message') == 'updated'){
									        	echo '<div class="alert alert-success">';
									            echo '<a class="close" data-dismiss="alert">×</a>';
									            echo '<strong>Success!</strong> Invoice sudah dibuat, anda akan mendapatkan informasi selanjutnya.<br/>';
									            echo '<a href="'.base_url().'">Close</a>';
									          	echo '</div>';       

									          	require_once('library-email/function.php');
									          	# email penerima
									          	# no penawaran
									          	# nama client
									          	# nama barang *
									          	# harga barang
									          	# harga total
									          	
									          	$to = $customer[0]['email'];
												$subject = "Invoice Mesin GARUDA Mechinery";
												// $message  = '<p>Isi dari Email Testing</p>';
												$message = "
													<div style='width: 700px;margin: auto;'>
														<div name='logo' >
												          <img style='width: 700px;margin: auto 0px;display: block;'' src='https://3.bp.blogspot.com/-JHRHSR7lvqg/WlzRj7ZL-DI/AAAAAAAAAns/kMQePxEx-eYdQ4fiW58iTUJcs-fugI4OACLcBGAs/s1600/logo.jpg'>
												        </div>
												        <div>
													        <div name='date' style='text-align: right;margin-bottom: 20px;'>
													        	<b>Malang, ".date('d m Y')." </b>
													        </div>
													        <div name='no_penawaran' style='margin-bottom: 20px;' >
													        	No : ".$no_penawaran."<br/>
																Hal : Invoice<br/>
													        </div>
													        <div name='no_penawaran' style='margin-bottom: 20px;font-size:20px; text-align:center;' >
													        	<b>INVOICE</b>
													        </div>
												          	<div name='ygTerhormat' style='margin-bottom: 20px;'>
																<b>
																	Kepada Yth : ".$customer[0]['nama_customer']."<br/>
																	Alamat : ".$customer[0]['alamat']."<br/>
																</b>
															</div>
															<div name='dgnHormat' style='margin-bottom: 20px;'>
																Dengan Hormat,<br/><br/>
																Dengan ini kami <b>GARUDA MACHINERY</b>, sebagai produsen dan distributor <i>Mesin-Mesin Food Processing dan Mesin - Mesin Teknologi Tepat Guna</i>, menawarkan mesin dengan perincian sebagai berikut :
															</div>
															<div name='barang' style='margin-bottom: 20px;'>
																<table border=1 style='width:100%;'>
																	<thead>
																		<tr>
																			<th>No. </th>
																			<th>Nama Mesin</th>
																			<th>Unit</th>
																			<th>Harga/Unit</th>
																			<th>Total</th>
																		</tr>
																	</thead>
																	<tbody>";
																	$i = 1;
																	foreach ($barang as $key => $value) {
																		
																		$total = $barang_qty[$key]['qty']*$barang_qty[$key]['harga'];
																		$message .= "
																			<tr>
																				<td style='text-align:center;'>".$i."</td>
																				<td>".$value['nama_barang']."</td>
																				<td style='text-align:center;'>".$barang_qty[$key]['qty']."</td>
																				<td style='text-align:right;'>".number_format($barang_qty[$key]['harga'], 0, ',', '.')."</td>
																				<td style='text-align:right;'>".number_format($total, 0, ',', '.')."</td>
																			</tr>
																		";
																		$i++;
																	}
																	$message .= "
																		<tr>
																			<td></td>
																			<td colspan='3' style='text-align:right;'>
																				<b>Grand Total</b>
																			</td>
																			<td style='text-align:right;'>
																				<b>".number_format($harga_total, 0, ',', '.')."<b>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</div>
															<div name='keterangan' style='margin-bottom: 20px;'>
																<b>Keterangan :</b><br/>
																<ul>
																	<li>Total pembelian sebesar <b>Rp. ".number_format($harga_total, 0, ',', '.')."</b></li>
																	<li>Harga ini adalah franco Malang. Ongkos kirim dan packing ditanggung pemesan. Harga dapat berubah sewaktu-waktu, dan untuk permintaan spesifikasi dan material yang berbeda.</li>
																	<li>Mesin buatan bengkel, pembuatan 1,5 bulan  dengan sistem pemesanan DP (Down Payment) sebesar   50 % dari harga mesin. Sisa pembayaran diberikan setelah barang siap kirim.</li>
																	<li>Mesin Import Stok menyesuaikan</li>
																	<li>Garansi		: 1 tahun service diluar spare part</li>
																	<li>Fasilitas	: Free Training dan Instalasi mesin di Malang. Luar Malang tambah akomodasi trainer.</li>
																	<li>Rekening <b>BCA</b> Cabang Malang  an. <b>HARLI NUGROHO 0111 887 011</b></li>
																	<li>Rekening <b>MANDIRI</b> cabang Malang a.n <b>HARLI NUGROHO. 144 0000 111 002</b></li>
																	<li>Bukti transfer dan alamat kirim lengkap mohon di <b>Fax</b> ke <b>0341 – 794 939</b></li>
																</ul>
																Demikian surat penawaran kami, atas perhatiannya kami ucapkan banyak terima kasih.
															</div>
															<div name='ttd' style='margin-bottom: 20px;'>
																<b>Hormat saya,</b><br/>
																<b>GARUDA MACHINERY</b><br/>
																<img style='width: 350px;' src='https://1.bp.blogspot.com/-9czCgh5zx-w/WlzRpg3FnyI/AAAAAAAAAnw/3_v-UQIpIUAbTDW2MeM_vLlf14a7_ndVwCLcBGAs/s320/ttd.jpg'><br/>
																<b>Wildan Abdul Aziz, ST.</b><br/>
																<i>Managing Director</i><br/>
																Email : <a>marketing@agenmesin.com</a><br/>
															</div>
												        </div>
													<div>";
												smtp_mail($to, $subject, $message, '', '', 0, 0, false, '');

									          	echo "<pre>";
									          	print_r($customer);
									          	print_r($no_penawaran);
									          	print_r($barang);
									          	print_r($barang_qty);
									          	print_r($harga_total);
									          	echo "</pre>";

									        }
									    }

				            			$attributes = array('class' => 'form-horizontal', 'id' => '');
					            		echo form_open_multipart('purchase/'.$this->uri->segment(2), $attributes);
				            			?>

				            			<input type="hidden" name="two" value="two">
				            			<input type="hidden" name="client" value="<?php echo $id_customer;?>">
				            			<input type="hidden" name="id_request" value="<?php echo $this->uri->segment(2);?>">
				            			<input type="hidden" name="no_penawaran" value="<?php echo $no_penawaran;?>">
				            			<table id="simple-table" class="table table-bordered table-hover">
											<thead>
												<tr>
													<th class="detail-col">No.</th>
													<th>Nama Barang</th>
													<th>Harga</th>
													<th>Qty</th>
												</tr>
											</thead>

											<tbody>
												<?php

												$i = 1;
												foreach ($barang as $key => $value) {
													# code...
													?>

													<tr>
														<td>#<?php echo $i;?></td>
														<td>
															<a href="<?php echo site_url("detail/").$value['id_barang']?>">
																<?php echo $value['nama_barang'];?>
															</a>
														</td>
														<td>
															<input type="text" disabled="disabled" name="harga" value="<?php echo $value['harga'];?>" style="text-align: center;">
														</td>
														<td>
															<input type="number" name="qty<?php echo $value['id_barang'];?>" style="text-align: center; width: 80px;" value="0" />
														</td>
													</tr>

													<?php
													$i++;
												}

												?>
											</tbody>
										</table>
										<button class="btn btn-outline btn-block popup-chat applozic-launcher" 
							              	style="background-color: #15C0C7; color: white;" 
							               	data-mck-id="80863">
											Purchase Barang
								        </button>

				            			<?php
				            			echo form_close();
				            		}
				            	}

				            	?>
				            </div>
				        </div>
				    </div>

					
				</div>
            </div>
		    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/product.js"></script>
		    <style>
		    	.controls input[type="text"], .controls input[type="email"]{

		    	    display: inline-block;
		    	    height: 35px;
    				margin-bottom: 0;
    				box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    				transition: border linear 0.2s, box-shadow linear 0.2s;
				    font-size: 13px;
				    line-height: 18px;
				    color: #555555;
				    border: 1px solid #ccc;
				    -webkit-border-radius: 3px;
				    -moz-border-radius: 3px;
				    border-radius: 3px;

		    	}
		    	label{
		    		font-weight: 500;
		    	}
			</style>

		</div>
</div>
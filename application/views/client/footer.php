<div class="container">
    <div class="row">
        <div class="col-xs-12">
        	<div class="box-separator-footer"></div>
    	</div>
	</div>
</div>

<div class="wrap-footer clearfix"> 
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-md-4">
				<div class="title-info-footer">
                    <h3>navigation</h3>
                </div>
                <div class="link-nav-footer">
                	<ul>
                		<li class='active '><a href='/'>Home</a></li>
        			</ul>
        		</div>
        	</div>
        	<div class="col-sm-4 col-md-4">
        		<div class="title-info-footer">
                    <h3>follow us</h3>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                    	<div class="link-social-media">

                        	<span class='st_facebook_large' displayText='Facebook'></span>
							<span class='st_twitter_large' displayText='Tweet'></span>
							<span class='st_linkedin_large' displayText='LinkedIn'></span>
							<span class='st_pinterest_large' displayText='Pinterest'></span>
							<span class='st_googleplus_large' displayText='Google +'></span>

							<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
							<script type="text/javascript">stLight.options({ publisher: "3a3d6d21-6ed4-436b-b3e7-20eb3259d76c", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>

							<br />
                                
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4">
				<div class="fm-title-content">
				    <h3>Garuda Machinery</h3>
				</div>
				<table class="m-view fm-short-contact">
					<tr>
			            <td>
			                <ul class='address_contact'>
			                	<li class='moq'>
			                		<span class=address>Alamat: </span>
			                	</li>
			                	<li class='moq'>
			                		<h5 class='no-margin-top no-margin-bottom'>Jl. Abd. Majid no. 107B Pakisjajar, Pakis Kab. Malang, Malang <br> Jawa Timur , Indonesia</h5>
			                	</li>
			                </ul>
			            </td>
			        </tr>
			        <tr>
			            <td>
			            	<div class='moq'>Telepon: </div>
			            	<div class='moq'>
			            		<span >
			            			<h5>
			            				<a >081333202221</a>
			            				<br />
			            				<a >0341794939</a>
			            				<br />
			            				<a >0341794385</a>
			            			</h5>
			            		</span>
			            	</div>
			            </td>
			            
			        </tr>
			    </table>

			</div>
		</div>
    </div>
</div>

<div style="clear: both;"></div>

</body>
</html>
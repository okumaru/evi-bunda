<div class="wrap-content-second clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/client/product.css" />
                	<script src="<?php echo base_url(); ?>assets/js/client/jssocials.min.js"></script>
				    <link href="<?php echo base_url(); ?>assets/css/client/style-jssocial.min.css" rel="stylesheet" />
				    <script src="<?php echo base_url(); ?>assets/js/client/react.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/react-dom.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/react-intl.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/browser.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/browser.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/jquery.dataTables.bootstrap.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/jquery.dataTables.min.js"></script>

                	<div class="row">
					    <div class="col-xs-12">
					        <ol class="breadcrumb fm-breadcrumb jm-breadcrumb">
					            <li>
					            	<a href="../">
					            		<i class="material-icons md-18">home</i>
					            	</a>
					            </li>
					            <li id="liProd" class="active" itemscope="" >
					                <a href="javascript:;" class="current">
					                    <span itemprop="title">Request</span>
					                </a>
					            </li>
					        </ol>
					    </div>
					</div>

					<div class="inner-content">
				        <div id="productDetail1">
				            <div class="row">
				            	<?php
				            	if (!empty($barang)) {
				            		
				            	 	?>
					             	<table id="simple-table" class="table table-bordered table-hover">
										<thead>
											<tr>
												<th class="detail-col">No.</th>
												<th>Nama Barang</th>
												<th>Type</th>
												<th>Harga</th>
												<th></th>
											</tr>
										</thead>

										<tbody>
											<?php

											$i = 1;
											foreach ($barang as $key => $value) {
												# code...
												?>

												<tr>
													<td>#<?php echo $i;?></td>
													<td>
														<a href="<?php echo site_url("detail/").$value[0]['id_barang']?>">
															<?php echo $value[0]['nama_barang'];?>
														</a>
													</td>
													<td><?php echo $value[0]['Type'];?></td>
													<td><?php echo $value[0]['harga'];?></td>
													<td class="hidden-480">
														<a href="<?php echo site_url("remove_product/").$value[0]['id_barang']?>">
															<span class="label label-sm label-warning">Delete From Request</span>
														</a>
													</td>
												</tr>

												<?php
												$i++;
											}

											?>
										</tbody>
									</table>


				            	 	<?php

				            	}else{

				            		echo "<div style='text-align:center;'>Request Kosong</div>";

				            	}
				            	$attributes = array('class' => 'form-horizontal', 'id' => '');
				            	echo form_open_multipart('penawaran', $attributes);
				            	$id_product = "";
				            	if (!empty($this->session->userdata('products'))) {
				            		
				            		$product = $this->session->userdata('products');
					            	foreach ($product as $key => $value) {
					            		
					            		if ($id_product) {
					            			$id_product .= ",";
					            		}
					            		$id_product .= $value;

					            	}

				            	

				            	?>
					            	<input type="hidden" name="products" value="<?php echo $id_product;?>">
					            	<div class="form-horizontal control-group">
					            		<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 
												Nama Customer 
											</label>
											<div class="col-sm-9 controls">
												<input type="text" id="form-field-1" name="nama" 
												placeholder="Nama Customer" class="col-xs-10 col-sm-5" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> 
												Alamat
											</label>
											<div class="col-sm-9 controls">
												<input type="text" id="form-field-2" name="alamat" 
												placeholder="Alamat" class="col-xs-10 col-sm-5" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-3"> 
												Email
											</label>
											<div class="col-sm-9 controls">
												<input type="email" id="form-field-3" name="email" 
												placeholder="Email" class="col-xs-10 col-sm-5" />
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-4"> 
												No. Telephone
											</label>
											<div class="col-sm-9 controls">
												<input type="number" id="form-field-4" name="tlpn" 
												placeholder="No. Telephone" class="col-xs-10 col-sm-5" />
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-4"> 
												Pesan
											</label>
											<div class="col-sm-9 controls">
												<textarea placeholder="Pesan" class="col-xs-10 col-sm-5" name="pesan" style="width: 375px; height: 400px"></textarea>
												
											</div>
										</div>


					            	</div>
					                <button class="btn btn-outline btn-block popup-chat applozic-launcher" 
					                	style="background-color: #15C0C7; color: white;" 
					                	data-mck-id="80863">
										Buat Permintaan 
						            </button>
					            <?php 
					            	}
					            echo form_close(); 
					            ?>
				            </div>
				        </div>
				    </div>
				</div>
            </div>
		    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/product.js"></script>
		    <script type="text/javascript">
		    	<?php 
		    		if ($added) {
		    			echo 'alert("Permintaan Sudah di Tambahkan");';
		    			echo 'window.location.href = "'.base_url().'"';
		    		}
		    	?>
		    </script>
		    <style>
		    	.controls input[type="text"], .controls input[type="email"]{

		    	    display: inline-block;
		    	    height: 35px;
    				margin-bottom: 0;
    				box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    				transition: border linear 0.2s, box-shadow linear 0.2s;
				    font-size: 13px;
				    line-height: 18px;
				    color: #555555;
				    border: 1px solid #ccc;
				    -webkit-border-radius: 3px;
				    -moz-border-radius: 3px;
				    border-radius: 3px;

		    	}
		    	label{
		    		font-weight: 500;
		    	}
			</style>

		</div>
</div>
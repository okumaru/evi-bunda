<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" lang="id">
<head id="Head1">
	
	<title>
	Garuda Machinery - Produsen Mesin Teknologi Tepat Guna
	</title>

	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/css/client/styles.min.css" rel="stylesheet" />
    <script src="<?php echo base_url(); ?>assets/js/client/scripts.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/client/jqueryvalidation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/client/jqueryvalidation/additional-methods.min.js"></script>
    <link href='<?php echo base_url(); ?>assets/css/client/dzone-dropdown-menu.css' rel="stylesheet"/>

    <script id="menuScriptContainer"></script>

    <script src="<?php echo base_url(); ?>assets/js/client/scripts-menu.min.js"></script>

    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo base_url(); ?>assets/css/client/style.css" />
    <link rel="canonical" href="http://www.garudamachinery.com/" />
    <link rel="alternate" hreflang="x-default" href="http://www.garudamachinery.com/" />
    <link rel="alternate" hreflang="en" href="http://en.garudamachinery.com/" />
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo base_url(); ?>assets/css/client//co26897.css" />
    <meta name="description" content="Garuda Machinery adalah toko mesin, produsen dan distributor mesin teknologi tepat guna. Jual mesin pertanian, mesin makanan, mesin pengemas dan lainnya." />


</head>
<body id="body">
	<script>
        var url = window.location.hostname;
        var cssRule =
    "color: red;" +
    "font-size: 55px;" +
    "font-weight: bold;" +
    "text-shadow: 1px 1px 5px rgb(0,0,0);" +
    "filter: dropshadow(color=rgb(249, 162, 34), offx=1, offy=1);";
        console.log("%c!STOP", cssRule);
        console.log("%c!This is a browser feature intended for developers.", "color:black;font-size: 20px;font-weight: bold;");

    </script>

    <div>
        <div class="back-top-button">
            <i class="glyphicon glyphicon-chevron-up">^</i>
        </div>
        <div id="top-menu-manager" class="new-top-menu">
            <div id="lang-mode">
                <div class="container">
                    <div class="clearfix">
                        <div class="pull-right text-right responsive-top-menu">

                            <div id="ctrlLang_liCart" class="link-top-cart">
                                <a rel="nofollow" href="<?php echo base_url(); ?>index.php/cart">
                                    <span class="text-cart">Keranjang Request</span>
                                    <span class="badge custom-badge-link kranjang" id="iCount">
                                        <?php echo count($this->session->userdata('products'));?>
                                    </span>
                                </a>

                            </div>
                            <div class="top-lang-new">
                                <div class="dropdown">
                                    
                                    <a href="###" class="btn btn-link" >
                                        <span class="label-languange">Indonesia</span>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>
                        .glyphicon-chevron-up:before{content:""}
                        .link-top-cart > a {
                            color: #333;
                        }

                            .link-top-cart > a:hover,
                            .link-top-cart > a:focus {
                                text-decoration: none;
                                color: #337ab7;
                            }

                        .link-top-cart {
                            padding-right: 15px;
                        }

                        .link-top-cart,
                        .top-lang-new {
                            display: inline-block;
                            vertical-align: middle;
                        }

                        .custom-badge-link {
                            position: relative;
                            top: -6px;
                        }

                        .keranjang {
                            top: 0 !important;
                            letter-spacing: 2px;
                            cursor: pointer;
                        }
                    </style>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-header clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row flexbox middle-xs">
                        <div class="last-xs last-sm first-md first-lg col-xs-8 col-sm-7 col-md-3 col-lg-3">
                            <div class="wrap-logo">
                                <div class="logo-container">
                                    <a href="<?php echo base_url(); ?>">
                                        <img alt="Logo Garuda Machinery" src="https://image1ws.indotrading.com/s3/co26897/companylogo/logo%20kecil.png" class="company-logo img-responsive" />
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-5 col-md-9 col-lg-9">
                            <style>
                                @media (min-width: 320px) and (max-width: 767px) {
                                    .closebtn {
                                        display: none;
                                    }
                                }

                                @media (min-width: 320px) and (max-width: 767px) {
                                    #dzone-dropdown-menu.jm-menu > ul > li.has-sub > a:after {
                                        display: none;
                                    }

                                    #dzone-dropdown-menu {
                                        background-color: #eaeaea;
                                        box-shadow: 3px 0 5px 0 rgba(138, 138, 138, 0.5);
                                        width: 0;
                                    }

                                    #dzone-dropdown-menu > ul > li {
                                        float: none;
                                    }

                                    #dzone-dropdown-menu.jm-menu > ul > li > a, #dzone-dropdown-menu > ul > li:hover > ul, #dzone-dropdown-menu ul ul li a {
                                        width: 100%;
                                    }

                                    #dzone-dropdown-menu .dropdown-toggle .caret {
                                        position: absolute;
                                        margin-top: 6px;
                                        right: -80px;
                                    }

                                    li.has-sub:hover ul {
                                        height: 100%;
                                        z-index: 999999;
                                        display: block;
                                        max-height: 500px;
                                    }

                                    li.has-sub ul {
                                        overflow: auto;
                                        max-height: 0;
                                        -webkit-transition: all 0.5s ease-out;
                                    }

                                    .sidenav {
                                        height: 100% !important;
                                        width: 0;
                                        position: fixed !important;
                                        z-index: 99999 !important;
                                        top: 0;
                                        left: 0;
                                        background-color: #111;
                                        overflow-x: hidden;
                                        transition: 0.5s;
                                        padding-top: 80px !important;
                                    }

                                    .sidenav a {
                                        padding: 8px 8px 8px 32px;
                                        text-decoration: none;
                                        font-size: 14px;
                                        color: #818181;
                                        display: block;
                                        transition: 0.3s;
                                    }

                                    .sidenav a:hover, .offcanvas a:focus {
                                        color: #f1f1f1;
                                    }

                                    .sidenav .closebtn {
                                        position: absolute !important;
                                        top: 18px;
                                        right: 5px;
                                        font-size: 29px;
                                        margin-left: 50px;
                                    }

                                    #dzone-dropdown-menu.jm-menu > ul > li > a {
                                        padding: 15px;
                                        display: inline-block;
                                        text-shadow: none;
                                        color: #000;
                                    }

                                    #dzone-dropdown-menu.jm-menu > ul > li.has-sub > a {
                                        white-space: nowrap;
                                        overflow: hidden;
                                        text-overflow: ellipsis;
                                        display: block;
                                        margin-right: 35px;
                                    }
                                }
                            </style>
                            <div id="dzone-dropdown-menu" class="jm-menu cssmenu sidenav" data-role="panel" data-position-fixed="true">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                <ul>
                                    <li class='active '>
                                        <a href='<?php echo base_url(); ?>'>Home</a>
                                    </li>
                                    <li class='active '>
                                        <a href='<?php echo site_url("purchase"); ?>'>Purchase</a>
                                    </li>
                                </ul>
                            </div>

                            <script>

                                if ($(window).width() < 320) {
                                    function openNav() {
                                        document.getElementById("dzone-dropdown-menu").style.width = "100%";
                                    }

                                    function closeNav() {
                                        document.getElementById("dzone-dropdown-menu").style.width = "0";
                                    }
                                }
                                else {
                                    function openNav() {
                                        document.getElementById("dzone-dropdown-menu").style.width = "320px";
                                    }

                                    function closeNav() {
                                        document.getElementById("dzone-dropdown-menu").style.width = "0";
                                    }
                                }


                                if ($(window).width() < 992) {
                                    (function ($) {
                                        $(document).ready(function () {
                                            $('#dzone-dropdown-menu.jm-menu.cssmenu > ul > li > a').click(function () {
                                                $('#dzone-dropdown-menu.jm-menu.cssmenu li').removeClass('active');
                                                $(this).closest('li').addClass('active');
                                                var checkElement = $(this).next();
                                                if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                                                    $(this).closest('li').removeClass('active');
                                                    checkElement.slideUp('normal');
                                                }
                                                if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                                                    $('#dzone-dropdown-menu.jm-menu.cssmenu ul ul:visible').slideUp('normal');
                                                    checkElement.slideDown('normal');
                                                }
                                                if ($(this).closest('li').find('ul').children().length == 0) {
                                                    return true;
                                                } else {
                                                    return false;
                                                }
                                            });
                                        });

                                        $(document).ready(function () {
                                            $("span.caret-mm.caret-menu-down").remove();
                                        });

                                    })(jQuery);
                                }

                            </script>
                            <div class="wrap-mobile-menu">
                                <span onclick="openNav()" class="mobile-menu"><i class="glyphicon glyphicon-menu-hamburger"></i></span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="wrap-banner clearfix">
	<div id="bannerservices">
        <div class="jm-slick-all owl-carousel">
        	<div class="jm-slick" style="height:400px;">
            	<img src="https://image2.indotrading.com/co26897/personalwebsite/635794702825386427.jpg" />
            </div>
           	<div class="jm-slick" style="height:400px;">
            	<img src="https://image2.indotrading.com/co26897/personalwebsite/635794702779990348.jpg" />
            </div>
            <div class="jm-slick" style="height:400px;">
            	<img src="https://image2.indotrading.com/co26897/personalwebsite/635794702690914191.jpg" />
            </div>
            <div class="jm-slick" style="height:400px;">
            	<img src="https://image2.indotrading.com/co26897/personalwebsite/635794702604022039.jpg" />
            </div>
        </div>
        <div class="customNavigation">
        	<a class="prev"></a>
            <a class="next"></a>
        </div>
    </div>
</div>

<div class="wrap-top-content clearfix">
    <div class="container">
    	<div class="row">
        	<div class="col-xs-12">
            	<div class="text-center desc-short-company">

                	<div class="col-xs-12 control-desc-member">
						<div class="fm-short-company flexbox middle-xs">
        					<h1 class="media-heading">Garuda Machinery</h1>
        					<div class='icon-verified-id'></div>
        				</div>
        				<p>Kami, Garuda Machinery Adalah Toko Mesin, Produsen, Dan Distributor Mesin-Mesin Teknologi Tepat Guna Yang Meliputi Mesin-Mesin Pertanian, Mesin-Mesin Makanan, Mesin-Mesin Pengemas, Dan Mesin-Mesin Industri Lainnya.<br /><br />Kami Memiliki Bengkel Mesin Dan Kantor Di Malang, Jawa Timur. Dengan Bekal Pengalaman Kami Akan Mesin-Mesin Usaha Dan Teknologi Tepat Guna, Kami Melayani Pemesanan, Pembuatan, Dan Rekayasa Mesin-Mesin Untuk Keperluan Usaha Dan Industri Anda.<br /><br />Selain Itu, Kami Juga Melayani Konsultasi Dan Rancang Bangun Untuk Mesin-Mesin Pengolahan Pangan Dan Hasil Pertanian Untuk Pengembangan Industri Rumah Tangga Dan Usaha Kecil Menengan (UKM) Baik Dari Instansi Pemerintahan, Perusahaan Swasta, Perorangan, Dan Tender.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="wrap-content-primary clearfix">
    <div class="container">

    	<div class="row">
        	<div class="col-xs-12">

            	<div class="title-content-master">
                	<h2>Product Kami</h2>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="wrap-content-primary clearfix">
    <div class="container">

        <div class="row">
            <div class="col-xs-12">

                <div class="boxparade clearfix">
                    <div class="row headparade" id="parade57811" data-paradeid="57811">
                        <div class="col-xs-12">
                            <div class="box-tab-category">
                                <div class="container-tab">
                                    <div class="row product-featured">
                                        <div class="col-xs-12 wrap-content-category">
                                            <div class="row tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="tab1" data-relid="tabparade57811">
                                                    <div class="slide-category clearfix" >
                                                        <?php
                                                            // echo "<pre>";
                                                            foreach ($barang as $key => $value) {
                                                                ?>
                                                                <div class="col-xs-3" style="margin-bottom: 20px;">
                                                                    <a href="<?php echo site_url("detail/").$value['id_barang']?>" class="thumbnail">
                                                                        <img class="img-responsive" src="<?php echo base_url()."uploads/".$barang[$key]['image']; ?>" alt="<?php echo $value['nama_barang'];?>">
                                                                        <div class="caption">
                                                                            <h3 class="product-name" style="height: 52px;">
                                                                                <?php echo $value['nama_barang'];?>
                                                                            </h3>
                                                                            <p class="newprice">
                                                                                <?php echo $value['harga'];?>
                                                                            </p>
                                                                        </div>
                                                                    </a>
                                                                </div>

                                                                <?php
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>

    </div>
</div>
<div class="wrap-content-second clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/client/product.css" />
                	<script src="<?php echo base_url(); ?>assets/js/client/jssocials.min.js"></script>
				    <link href="<?php echo base_url(); ?>assets/css/client/style-jssocial.min.css" rel="stylesheet" />
				    <script src="<?php echo base_url(); ?>assets/js/client/react.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/react-dom.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/react-intl.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/browser.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/browser.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/jquery.dataTables.bootstrap.min.js"></script>
				    <script src="<?php echo base_url(); ?>assets/js/client/jquery.dataTables.min.js"></script>

                	<div class="row">
					    <div class="col-xs-12">
					        <ol class="breadcrumb fm-breadcrumb jm-breadcrumb">
					            <li>
					            	<a href="/">
					            		<i class="material-icons md-18">home</i>
					            	</a>
					            </li>
					            <li id="liProd" class="active" itemscope="" >
					                <a href="javascript:;" class="current">
					                    <span itemprop="title">Penawaran</span>
					                </a>
					            </li>
					        </ol>
					    </div>
					</div>
					<?php

					if($error == 1){
						
						// redirect('cart');
						?>
						<a href="<?php echo site_url("cart"); ?>">
							<button class="btn btn-outline btn-block popup-chat applozic-launcher" style="background-color: #15C0C7; color: white;" data-mck-id="80863">
										Backup To Cart
						            </button>
						</a>
						<script type="text/javascript">
							// alert('Lengkapi Fill terlebih dahulu');
							// var txt;
							// var r = confirm("Lengkapi Fill terlebih dahulu");
							// if (r == true) {
							    // txt = "You pressed OK!";
							    
							// } else {
								
							    // txt = "You pressed Cancel!";
							// }
							// alert("Hello! I am an alert box!!");
						</script>

		               	<!-- <div class="inner-content">
						    <div id="productDetail1">
						        <div class="row">
						           	
						        </div>
						    </div>
						</div> -->
		               	<?php

		            }else{

		            	?>
		            	<div class="inner-content">
						    <div id="productDetail1">
						        <div class="row">
						           	Terima kasih telah membuat permintaan, kami akan mengirimi anda Penawaran Produk dalam waktu 4x12 jam maksimal ke alamat email yang sudah anda daftarkan .<br/>
						           	Silakan anda cek di inbox atau folder spam anda secara berkala.
						        </div>
						    </div>
						</div>
		            	<?php

		            }

					?>
					
				</div>
            </div>
		    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/product.js"></script>
		    <style>
		    	
			</style>

		</div>
</div>
<!DOCTYPE html> 
<html lang="en-US">
<head>

  <title><?php echo $title;?></title>
  <meta charset="utf-8">

	<meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="<?php echo base_url(); ?>assets/css/admin/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/admin/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="<?php echo base_url(); ?>assets/css/admin/demo.css" rel="stylesheet" />
    <style type="text/css">
        tbody{
            font-size: 14px;
        }
    </style>

</head>
<body>
    <div class="wrapper">
        <div class="sidebar" data-image="<?php echo base_url(); ?>assets/img/sidebar-5.jpg">
        	<div class="sidebar-wrapper">
                <div class="logo">
                    <a href="http://www.creative-tim.com" class="simple-text">
                        Garuda Machinery
                    </a>
                </div>
                <ul class="nav">
                    <!-- <li>
                        <a class="nav-link" href="#">
                            <i class="nc-icon nc-planet"></i>
                            <p>Dashboard</p>
                        </a>
                    </li> -->
                    <li>
                        <a class="nav-link" href="<?php echo site_url('admin/customer'); ?>">
                            <i class="nc-icon nc-single-02"></i>
                            <p>Customer</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="<?php echo site_url('admin/barang'); ?>">
                            <i class="nc-icon nc-app"></i>
                            <p>Mesin</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="<?php echo site_url('admin/request'); ?>">
                            <i class="nc-icon nc-email-83"></i>
                            <p>Request</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="<?php echo site_url('admin/penawaran'); ?>">
                            <i class="nc-icon nc-send"></i>
                            <p>Penawaran</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="<?php echo site_url('admin/po'); ?>">
                            <i class="nc-icon nc-lock-circle-open"></i>
                            <p>Purchase</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class=" container-fluid  ">
                    <a class="navbar-brand" href="#pablo"> <?php echo strtoupper($this->uri->segment(2));?> </a>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo site_url('admin/logout'); ?>">
                                    <span class="no-icon">Log out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            
            
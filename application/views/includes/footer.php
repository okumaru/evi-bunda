				<footer class="footer">
	                <div class="container">
	                    <nav>
	                        <ul class="footer-menu">
	                            <li>
	                                <a href="<?php echo base_url(); ?>">
	                                    Visit Page
	                                </a>
	                            </li>
	                        </ul>
	                        <p class="copyright text-center">
	                            ©
	                            <script>
	                                document.write(new Date().getFullYear())
	                            </script>
	                            <a href="#">Garuda Mechinery</a>, Administrator Management
	                        </p>
	                    </nav>
	                </div>
	            </footer>
	        </div>
		</div>
	</body>
	<!--   Core JS Files   -->
	<script src="<?php echo base_url(); ?>assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/core/popper.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/core/bootstrap.min.js" type="text/javascript"></script>
	<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
	<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-switch.js"></script>
	<!--  Google Maps Plugin    -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
	<!--  Chartist Plugin  -->
	<script src="<?php echo base_url(); ?>assets/js/plugins/chartist.min.js"></script>
	<!--  Notifications Plugin    -->
	<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-notify.js"></script>
	<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
	<script src="<?php echo base_url(); ?>assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<script src="<?php echo base_url(); ?>assets/js/demo.js"></script>

</html>
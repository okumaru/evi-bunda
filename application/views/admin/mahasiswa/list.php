    <div class="container top">

      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin/mahasiswa"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(2));?> 
          <a  href="<?php echo site_url("admin/mahasiswa/add") ?>" class="btn btn-success">Add a new</a>
        </h2>
      </div>
      
      <div class="row">
        <div class="span12 columns">

          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="header">NIM</th>
                <th class="header">Nama</th>
                <th class="header">Status</th>
                <th class="header">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($mhs as $row)
              {
                echo '<tr>';
                echo '<td>'.$row['nim'].'</td>';
                echo '<td>'.$row['nama'].'</td>';
                if ($row['status'] == 0) {
                  echo '<td>Belum Memilih</td>';
                }else{
                  echo '<td>Sudah Memilih</td>';
                }
                
                echo '<td class="crud-actions">
                  <a href="'.site_url("admin/mahasiswa/update/").$row['nim'].'" class="btn btn-info">view & edit</a>  
                  <a href="'.site_url("admin/mahasiswa/delete/").$row['nim'].'" class="btn btn-danger">delete</a>
                </td>';
                echo '</tr>';
              }
              ?>      
            </tbody>
          </table>

          <?php //echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>

      </div>
    </div>
<?php

$attributes = array('class' => 'form-horizontal', 'id' => '');
echo form_open('admin/po/add/', $attributes);

?>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Detail Penawaran</h4>
            <p class="card-category">Detail Dari Penawaran yang sudah dibuat PO </p>
          </div>
          <div class="card-body">
            <!-- <form> -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>No. Penawaran</label>
                    <input type="text" class="form-control" disabled="disabled" value="<?php echo $penawaran[0]['no_penawaran']; ?>">
                    <input type="hidden" class="form-control" name="no_penawaran" value="<?php echo $penawaran[0]['no_penawaran']; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label>Nama Customer</label>
                    <input type="text" class="form-control" value="<?php echo $penawaran[0]['nama_customer']; ?>">
                    <!-- <input type="text" class="form-control" value="<?php echo $customer[0]['alamat']; ?>"> -->
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label>Tanggal</label>
                    <input type="text" class="form-control" value="<?php echo $penawaran[0]['tanggal']; ?>">
                    <!-- <input type="text" class="form-control" value="<?php echo $customer[0]['email']; ?>"> -->
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>List Barang</label><br/>
                    <p style="font-size: 14px;color: darkslateblue;">Silakan isikan kosong jumlah yang di order jika barang tidak dimasukkan dalam PO!!</p>

                    <?php

                      // echo "<pre>";
                      // print_r($penawaran[0]['barang_new']);
                      // echo "</pre>";
                      $id_barang = "";
                      foreach ($penawaran[0]['barang_new'] as $key => $value) {
                        
                        if ($key != 1) {
                          $id_barang .= ",";
                        }
                        $id_barang .= $value['id'];
                        ?>
                        <b>Barang <?php echo $key;?></b>
                        <div class="row">
                          <div class="col-md-6 pr-1">
                            <div class="form-group">
                              <label>Nama Barang</label>
                              <input type="text" class="form-control" value="<?php echo $value['nama_barang']; ?>">
                            </div>
                          </div>
                          <div class="col-md-3 pl-1">
                            <div class="form-group">
                              <label>Jumlah Barang Yang Di Order</label>
                              <input type="number" class="form-control" name="qty<?php echo $value['id']?>" value="0" style="text-align: center;">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 pr-1">
                            <div class="form-group">
                              <label>Harga Baru</label>
                              <input type="text" disabled="disabled" class="form-control" name="harga<?php echo $value['id']?>" value="<?php echo $value['harga_baru']; ?>">
                              <input type="hidden" class="form-control" name="harga<?php echo $value['id']?>" value="<?php echo $value['harga_baru']; ?>">
                            </div>
                          </div>
                          <div class="col-md-6 pl-1">
                            <div class="form-group">
                              <label>Harga Lama</label>
                              <input type="text" disabled="disabled" class="form-control" value="<?php echo $value['harga_lama']; ?>">
                            </div>
                          </div>
                        </div>
                        <hr>

                        <?php

                      }
                      
                    ?>
                    <input type="hidden" name="barang" value="<?php echo $id_barang; ?>">
                  </div>
                </div>
              </div>
              <a href="<?php echo site_url("admin/penawaran"); ?>">
                <!-- <p class="btn btn-primary" type="reset">Back</p> -->
                <button type="submit" class="btn btn-info btn-fill pull-right">Back</button>
              </a>
              <!-- <button type="submit" class="btn btn-info btn-fill pull-right" style="margin-right: 10px;">Buat PO</button> -->
              <div class="clearfix"></div>
            <!-- </form> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
            <h4 class="card-title">Penawaran</h4>
            <p class="card-category">List Penawaran</p>
          </div>
          <div class="card-body table-full-width table-responsive">
            <table class="table table-hover table-striped">
              <thead>
                <th>No Penawaran</th>
                <th>Tanggal</th>
                <th>Client</th>
                <th></th>
              </thead>
              <tbody>
                <?php

                foreach ($penawaran as $key => $value) {
                  # code...
                  echo "
                  <tr>
                    <td>".$value['no_penawaran']."</td>
                    <td>".$value['tanggal']."</td>
                    <td>".$value['nama_customer']."</td>
                    <td>
                      <a href=".site_url("admin/penawaran/detail/").$value['id_request']." class='btn btn-info' style='margin-right:10px;'>Detail</a>  
                    </td>
                  </tr>
                  ";
                }

              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
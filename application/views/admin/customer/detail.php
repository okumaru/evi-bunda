<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Detail Customer</h4>
          </div>
          <div class="card-body">
            <!-- <form> -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" value="<?php echo $customer[0]['nama_customer']; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" class="form-control" value="<?php echo $customer[0]['alamat']; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control" value="<?php echo $customer[0]['email']; ?>">
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label>Tlpn</label>
                    <input type="text" class="form-control" value="<?php echo $customer[0]['tlp']; ?>">
                  </div>
                </div>
              </div>
              <a href="<?php echo site_url("admin/customer"); ?>">
                <!-- <p class="btn btn-primary" type="reset">Back</p> -->
                <button type="submit" class="btn btn-info btn-fill pull-right">Back</button>
              </a>
              <div class="clearfix"></div>
            <!-- </form> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
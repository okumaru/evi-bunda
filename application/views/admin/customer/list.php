<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
            <h4 class="card-title">Customer</h4>
            <p class="card-category">List Customer Yang Melakukan Request Penawaran</p>
          </div>
          <div class="card-body table-full-width table-responsive">
            <table class="table table-hover table-striped">
              <thead>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Tlpn</th>
                <th>Email</th>
                <th></th>
              </thead>
              <tbody>
                <?php

                foreach ($customer as $key => $value) {
                  # code...
                  echo "
                  <tr>
                    <td>".$value['nama_customer']."</td>
                    <td>".$value['alamat']."</td>
                    <td>".$value['tlp']."</td>
                    <td>".$value['email']."</td>
                    <td>
                      <a href=".site_url("admin/customer/detail/").$value['id_customer']." class='btn btn-info'>Detail</a>  
                    </td>
                  </tr>
                  ";
                }

              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
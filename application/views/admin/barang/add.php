    <!-- <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin/"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">Add</a>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Add <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      </div>
      
      <?php
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == 'updated')
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> Barang Added with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }

      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');

      //form validation
      echo validation_errors();

      echo form_open_multipart('admin/barang/add/', $attributes);
      ?>
        <fieldset>
          <div class="control-group">
            <label for="inputError" class="control-label">Type :</label>
            <div class="controls">
              <p style="padding-top: 5px; margin: 0px;">
                <input type="text" id="" name="type" value="" >
              </p>
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Nama Barang :</label>
            <div class="controls">
              <p style="padding-top: 5px; margin: 0px;">
                <input type="text" id="" name="nama_barang" value="" >
              </p>
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Image :</label>
            <div class="controls">
              <p style="padding-top: 5px; margin: 0px;">
                <input type="file" name="image" size="20" />
                <br/>
              </p>
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Spesifikasi :</label>
            <div class="controls">
              <p style="padding-top: 5px; margin: 0px;">
                <textarea name="spesifikasi" style="width: 400px; height: 400px"></textarea>
              </p>
              
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Harga :</label>
            <div class="controls">
              <p style="padding-top: 5px; margin: 0px;">
                <input type="text" id="" name="harga" value="" >
              </p>
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save</button>
            <a href="<?php echo site_url("admin/barang"); ?>">
              <p class="btn" type="reset">Back</p>
              <!-- <button class="btn" type="reset">Cancel</button> 
            </a>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div> -->
     
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Tambahkan Mesin</h4>
          </div>
          <div class="card-body">
            
            <?php

            if($this->session->flashdata('flash_message')){
              if($this->session->flashdata('flash_message') == 'updated')
              {
                echo '<div class="alert alert-success">';
                  echo '<a class="close" data-dismiss="alert">×</a>';
                  echo '<strong>Well done!</strong> Barang Added with success.';
                echo '</div>';       
              }else{
                ?>
                
                <?php
                echo '<div class="alert alert-danger">';
                  echo '<a class="close" data-dismiss="alert">×</a>';
                  echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
                echo '</div>';          
              }
            }

            //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');

      //form validation
      echo validation_errors();

      echo form_open_multipart('admin/barang/add/', $attributes);

            ?>

              <div class="row">
                <div class="col-md-4 pr-1">
                  <div class="form-group">
                    <label>Type</label>
                    <input type="text" name="type" class="form-control" placeholder="XX-0001">
                  </div>
                </div>
                <div class="col-md-4 px-1">
                  <div class="form-group">
                    <label>Harga Mesin</label>
                    <input type="text" name="harga" class="form-control" placeholder="1400000">
                  </div>
                </div>
                <div class="col-md-4 pl-1">
                  <div class="form-group">
                    <label>Nama Mesin</label>
                    <input type="text" name="nama_barang" class="form-control" placeholder="Nama Mesin">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><br/>
                    <!-- <input type="text" class="form-control"> -->
                    <input type="file" name="image" size="20" />
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Spesifikasi</label>
                    <textarea rows="4" cols="80" name="spesifikasi" style="height: 100px" class="form-control" placeholder="Spesifikasi Mesin"></textarea>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-info btn-fill pull-right">Submit</button>
              <div class="clearfix"></div>
            
            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
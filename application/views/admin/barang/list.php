<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
            <div class="row">
              <div class="col-md-6 pr-1">
                <h4 class="card-title">Barang</h4>
                <p class="card-category">List Barang Yang Bisa di Buat Penawaran</p>
              </div>
              <div class="col-md-6 pl-1">
                <a href="<?php echo site_url("admin/barang/add")?>">
                  <button type="submit" style="margin-top: 6px;" class="btn btn-info btn-fill pull-right">
                    Tambahkan Barang
                  </button>
                </a>
              </div>
            </div>
          </div>
          <div class="card-body table-full-width table-responsive">
            <table class="table table-hover table-striped">
              <thead>
                <th>Nama Barang</th>
                <th>Spesifikasi</th>
                <th>Harga</th>
                <th></th>
              </thead>
              <tbody>
                <?php

                foreach ($barang as $key => $value) {
                  # code...
                  echo "
                  <tr>
                    <td>".$value['nama_barang']."</td>
                    <td>".$value['Spesifikasi']."</td>
                    <td>".$value['harga']."</td>
                    <td>
                      <a href=".site_url("admin/barang/update/").$value['id_barang']." class='btn btn-info'>view & edit</a>  
                      <a href=".site_url("admin/barang/delete/").$value['id_barang']." class='btn btn-danger'>delete</a>
                    </td>
                  </tr>
                  ";
                }

              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
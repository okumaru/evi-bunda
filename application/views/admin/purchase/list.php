<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
            <h4 class="card-title">Purchase</h4>
            <p class="card-category">List Purchase yang sudah dibuat</p>
          </div>
          <div class="card-body table-full-width table-responsive">
            <table class="table table-hover table-striped">
              <thead>
                <th class="header">No. Penawaran</th>
                <th class="header">Nama Customer</th>
                <th class="header">Tanggal</th>
                <th class="header"></th>
              </thead>
              <tbody>
                <?php

                foreach ($purchase as $key => $value) {
                  # code...
                  echo "
                  <tr>
                    <td>".$value['no_penawaran']."</td>
                    <td>".$value['nama_customer']."</td>
                    <td>".$value['tanggal']."</td>
                    <td>
                      <a href=".site_url("admin/po/detail/").$value['id']." class='btn btn-info'>Detail</a>  
                    </td>
                  </tr>
                  ";
                }

              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
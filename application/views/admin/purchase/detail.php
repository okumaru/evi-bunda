<!--     <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin/"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">Detail</a>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Detail <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      </div>
      
      <?php
      // echo "<pre>";
      // print_r($purchase);
      // echo "</pre>";

      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');

      //form validation
      // echo validation_errors();

      echo form_open('admin/purchase/update/'.$this->uri->segment(4).'', $attributes);
      ?>
        <fieldset>
          <div class="control-group">
            <label for="inputError" class="control-label">No. Request :</label>
            <div class="controls">
              <p style="padding-top: 5px; margin: 0px;">
                <a href="<?php echo site_url("admin/request/detail/").$purchase[0]['kode_offer']; ?>">
                  #<?php echo $purchase[0]['kode_offer']; ?>
                </a>
              </p>
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Nama Customer :</label>
            <div class="controls">
              <p style="padding-top: 5px; margin: 0px;">
                <a href="<?php echo site_url("admin/customer/detail/").$purchase[0]['id_customer']; ?>">
                  <?php echo $purchase[0]['nama_customer']; ?>
                </a>
              </p>
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Barang :</label>
            <div class="controls">
              <table border=1>
                <tr>
                  <th>Nama Barang</th>
                  <th>Qty</th>
                  <th>Harga Satuan</th>
                  <th>Harga Total</th>
                </tr>
                <?php

                  foreach ($purchase[0]['barang'] as $key => $value) {
                    # code...
                    ?>
                    <tr>
                      <td>
                        <a href="<?php echo site_url("admin/barang/update/").$value['id']; ?>">
                          <?php echo $value['nama_barang'];?>
                        </a>
                      </td>
                      <td><?php echo $value['qty'];?></td>
                      <td>Rp. <?php echo $value['harga'];?></td>
                      <td>Rp. <?php echo $value['total_harga'];?></td>
                    </tr>
                    <?php
                  }

                ?>
              </table>
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Tanggal :</label>
            <div class="controls">
              <p style="padding-top: 5px; margin: 0px;">
                <?php echo $purchase[0]['tanggal']; ?>
              </p>
            </div>
          </div>
          <div class="form-actions">
            <!-- <button class="btn btn-primary" type="submit">Save changes</button> 
            <a href="<?php echo site_url("admin/po"); ?>">
              <p class="btn btn-primary" type="reset">Back</p>
              <!-- <button class="btn" type="reset">Cancel</button> 
            </a>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
      -->

<?php
// echo "<pre>";
// print_r($purchase);
// echo "</pre>";
?>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Detail Purchase</h4>
          </div>
          <div class="card-body">
            <!-- <form> -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>No Penawaran</label>
                    <input type="text" class="form-control" value="<?php echo $purchase[0]['no_penawaran']; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" value="<?php echo $purchase[0]['nama_customer']; ?>">
                    <!-- <input type="text" class="form-control" value="<?php echo $customer[0]['alamat']; ?>"> -->
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label>Tanggal</label>
                    <input type="text" class="form-control" value="<?php echo $purchase[0]['tanggal']; ?>">
                    <!-- <input type="text" class="form-control" value="<?php echo $customer[0]['email']; ?>"> -->
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>List Barang</label><br/>
                    <?php
                      // echo "<pre>";
                      // print_r($purchase[0]['barang']);
                      // echo "</pre>";
                      // die();
                      $i = 1;
                      $barang = "";
                      foreach ($purchase[0]['barang'] as $key => $value) {
                        if (!empty($barang)) {
                          $barang .= ",";
                        }
                        $barang .= $value['id'];

                        ?>
                        <b>Barang <?php echo $key;?></b>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Nama Barang</label>
                              <input type="text" class="form-control" value="<?php echo $value['nama_barang']; ?>">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 pr-1">
                            <div class="form-group">
                              <label>Harga Penawaran</label>
                              <input type="text" class="form-control" value="<?php echo $value['harga']; ?>">
                            </div>
                          </div>
                          <div class="col-md-6 pl-1">
                            <div class="form-group">
                              <label>Jumlah Barang</label>
                              <input type="text" class="form-control" value="<?php echo $value['qty']; ?>">
                            </div>
                          </div>
                        </div>
                        
                        <?php
                        $i++;
                      }
                    ?>
                  </div>
                </div>
              </div> 
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Total Harga</label>
                    <input type="text" class="form-control" value="<?php echo $value['total_harga']; ?>">
                  </div>
                </div>
              </div>
              <a href="<?php echo site_url("admin/po") ?>">
                <!-- <p class="btn btn-primary" type="reset">Back</p> -->
                <button type="submit" class="btn btn-info btn-fill pull-right" style="margin-right: 10px;">Back</button>
              </a>
              <div class="clearfix"></div>
            <!-- </form> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
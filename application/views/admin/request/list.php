<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card strpied-tabled-with-hover">
          <div class="card-header ">
            <h4 class="card-title">Request</h4>
            <p class="card-category">List Request Penawaran Yang di Buat Customer</p>
          </div>
          <div class="card-body table-full-width table-responsive">
            <table class="table table-hover table-striped">
              <thead>
                <th>No. Request</th>
                <th>Nama Customer</th>
                <th>Email</th>
                <th>Tanggal</th>
                <th></th>
              </thead>
              <tbody>
                <?php

                foreach ($request as $key => $value) {
                  # code...
                  echo "
                  <tr>
                    <td>".$value['kode_offer']."</td>
                    <td>".$value['nama_customer']."</td>
                    <td>".$value['email']."</td>
                    <td>".$value['tanggal']."</td>
                    <td>
                      <a href=".site_url("admin/request/detail/").$value['kode_offer']." class='btn btn-info'>Detail</a>  
                    </td>
                  </tr>
                  ";
                }

              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    <?php

      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
      echo form_open('request/view_penawaran', $attributes);

      $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
      $date_terdaftar = explode("-", $request[0]['tanggal']);
      $bulan = ltrim($date_terdaftar[1], '0');
      $tahun = substr($date_terdaftar[0], -2);
      $no_penawaran = str_pad($request[0]['kode_offer'], 3, '0', STR_PAD_LEFT)."/SP/GM/".$array_bulan[$bulan]."/".$tahun;
      
    ?>

<input type="hidden" name="no_penawaran" value="<?php echo $no_penawaran;?>">
<input type="hidden" name="id_request" value="<?php echo $request[0]['kode_offer'];?>">
<input type="hidden" name="id_client" value="<?php echo $request[0]['id_customer'];?>">
<input type="hidden" name="nama_client" value="<?php echo $request[0]['nama_customer'];?>">
<input type="hidden" name="email_client" value="<?php echo $request[0]['email'];?>">

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Detail Request</h4>
          </div>
          <div class="card-body">
            <!-- <form> -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>No. Request</label>
                    <input type="text" class="form-control" value="<?php echo $request[0]['kode_offer']; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" value="<?php echo $request[0]['nama_customer']; ?>">
                    <!-- <input type="text" class="form-control" value="<?php echo $customer[0]['alamat']; ?>"> -->
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label>Tanggal</label>
                    <input type="text" class="form-control" value="<?php echo $request[0]['tanggal']; ?>">
                    <!-- <input type="text" class="form-control" value="<?php echo $customer[0]['email']; ?>"> -->
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Pesan</label>
                    <input type="text" class="form-control" value="<?php echo $request[0]['pesan']; ?>">
                    <!-- <input type="text" class="form-control" value="<?php echo $customer[0]['tlp']; ?>"> -->
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>List Barang</label><br/>
                    <?php
                    $i = 1;
                    $barang = "";
                    foreach ($request[0]['barang'] as $key => $value) {
                      if (!empty($barang)) {
                        $barang .= ",";
                      }
                      $barang .= $value[0]['id_barang'];

                      ?>
                      <input type="hidden" name="image<?php echo $value[0]['id_barang']?>" value="<?php echo $value[0]['image']?>">
                      <div class="control-group">
                        <b>Barang <?php echo $i;?></b>
                      </div>
                      <div class="row">
                        <div class="col-md-6 pr-1">
                          <div class="form-group">
                            <label>Nama Barang</label>
                            <input type="text" class="form-control" value="<?php echo $value[0]['nama_barang']; ?>">
                            <input type="hidden" name="nama_barang<?php echo $value[0]['id_barang']?>" value="<?php echo $value[0]['nama_barang'];?>">
                          </div>
                        </div>
                        <div class="col-md-6 pl-1">
                          <div class="form-group">
                            <label>Harga Penawaran</label>
                            <!-- <input type="number" class="form-control" name="qty<?php echo $value['id']?>" value="0" style="text-align: center;"> -->
                            <input type="text" class="form-control" name="harga<?php echo $value[0]['id_barang']?>" value="<?php echo $value[0]['harga']?>">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Spesifikasi Barang</label>
                              <textarea class="form-control" style='height: 250px'><?php echo $value[0]['Spesifikasi'];?></textarea>
                              <input type="hidden" name="spesifikasi_barang<?php echo $value[0]['id_barang']?>" value="<?php echo $value[0]['Spesifikasi'];?>">
                            </div>
                          </div>
                          
                        </div>
                      
                      <br/><hr>
                      
                      <?php
                      $i++;
                    }
                  ?>
                  <input type="hidden" name="barang" value="<?php echo $barang;?>">
                  </div>
                </div>
              </div>
              <a href="<?php echo site_url("admin/request");?>">
                <div class="btn btn-info btn-fill pull-right">Back</div>
                <!-- <button class="btn btn-info btn-fill pull-right">Back</button> -->
              </a>
              <button type="submit" class="btn btn-info btn-fill pull-right" style="margin-right: 10px;">Buat Penawaran</button>
              <!-- <input class="btn btn-primary" type="submit" name="submit" value="Buat Penawaran"> -->
              <div class="clearfix"></div>
            <!-- </form> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
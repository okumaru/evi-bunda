<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Detail Request</h4>
          </div>
          <div class="card-body">
            <!-- <form> -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>No. Request</label>
                    <input type="text" class="form-control" value="<?php echo $request[0]['kode_offer']; ?>">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 pr-1">
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" value="<?php echo $request[0]['nama_customer']; ?>">
                    <!-- <input type="text" class="form-control" value="<?php echo $customer[0]['alamat']; ?>"> -->
                  </div>
                </div>
                <div class="col-md-6 pl-1">
                  <div class="form-group">
                    <label>Tanggal</label>
                    <input type="text" class="form-control" value="<?php echo $request[0]['tanggal']; ?>">
                    <!-- <input type="text" class="form-control" value="<?php echo $customer[0]['email']; ?>"> -->
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Pesan</label>
                    <input type="text" class="form-control" value="<?php echo $request[0]['pesan']; ?>">
                    <!-- <input type="text" class="form-control" value="<?php echo $customer[0]['tlp']; ?>"> -->
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>List Barang</label><br/>
                    <?php
                      // echo "<pre>";
                      // print_r($request[0]['barang']);
                      // echo "</pre>";
                      // die();
                      foreach ($request[0]['barang'] as $key => $value) {

                        if ($key >= 1) { 
                          echo ", ";
                        }
                        echo "<a href='".site_url('admin/barang/update/').$value[0]['id_barang']."'>".$value[0]['nama_barang']."</a>";
                      }
                    ?>
                    <!-- <input type="text" class="form-control" value="<?php echo $request[0]['pesan']; ?>"> -->
                    <!-- <input type="text" class="form-control" value="<?php echo $customer[0]['tlp']; ?>"> -->
                  </div>
                </div>
              </div>
              <a href="<?php echo site_url("admin/request"); ?>">
                <!-- <p class="btn btn-primary" type="reset">Back</p> -->
                <button type="submit" class="btn btn-info btn-fill pull-right">Back</button>
              </a>
              <a href="<?php echo site_url("admin/request/penawaran/").$this->uri->segment(4); ?>">
                <button type="submit" class="btn btn-info btn-fill pull-right" style="margin-right: 10px;">Buat Penawaran</button>
              </a>
              <div class="clearfix"></div>
            <!-- </form> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
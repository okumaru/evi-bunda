<html>
	<head>
		<title><?php echo $title;?></title>
		<meta charset="utf-8">
		<link href="<?php echo base_url(); ?>assets/css/mhs/global.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div>
	      <?php 
	      $attributes = array('class' => 'form-signin');
	      echo form_open('mahasiswa/login/validate_credentials', $attributes);
	      
	      ?>
	      	<div class="form-login">
	      		<div style="color: #fff; text-align: center; margin: 25px auto; ">
	      			<i>Stimata.ac.id</i>
	      		</div>
	      		<div class="control-group">
		        	<div class="controls">
		            	<input type="text" class="loginFrom" id="" name="nim" value="" placeholder="NIM" >
		              	<!--<span class="help-inline">Woohoo!</span>-->
		            </div>
		        </div>
		        <div class="control-group">
		            <div class="controls">
		            	<input type="password" style="width: 225px; padding: 10px; border: 0px; border-radius: 4px;" name="pass" value="" placeholder="PASSWORD" >
		            	<!--<span class="help-inline">Woohoo!</span>-->
		            </div>
		        </div>
		        <div class="form-actions">
		            <button class="btn btn-primary" type="submit">LOGIN</button>
		        </div>
	      	</div>
        <?php echo form_close(); ?>
	    </div><!--container-->
	    <script src="<?php echo base_url(); ?>assets/js/jquery-1.7.1.min.js"></script>
	    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	</body>
</html>
<html>
	<head>
		<title><?php echo $title;?></title>
		<link href="<?php echo base_url(); ?>assets/css/mhs/new.css" rel="stylesheet" type="text/css">
		<!-- <link href="<?php echo base_url(); ?>assets/css/mhs/global.css" rel="stylesheet" type="text/css"> -->
	</head>
	<script type="text/javascript">
		function myFunction(i){
			// alert("Hello! I am an alert box!!");
			var radio = document.getElementById(i);
			radio.checked = true;
		}
	</script>
	<body>
		<div>
			<div class="header">
				<img src="http://localhost/CodeIgniter/assets/images/banner_dual_logo_360.png" />
				<p>Jl. Laksda Adi Sucipto 249A Blimbing, Malang, Jawa Timur</p>
			</div>
			<?php 
		    $attributes = array('class' => 'form-signin');
		    echo form_open('mahasiswa/choose', $attributes);
		      
		    ?>
		    <!-- <input type="hidden" name="nim" value="<?php echo $mhs;?>"> -->
		    <?php
		    // echo "<pre>";
		    // print_r($kandidat);
		    foreach ($kandidat as $key => $value) {

		    	$radio = "radio-".$key;
		    	?>
		    	<div class="l groupKandidat">
		    		<div class="border" id="border-<?php echo $key;?>" onclick="myFunction('<?php echo $radio?>')">
		    			<div class="l casual">
			    			<img src="<?php echo $value['foto_ketua']?>" >
			    		</div>
			    		<div class="l casual">
			    			<img src="<?php echo $value['foto_wakil']?>" >
			    		</div>
			    		<p class="cl" ></p>
		    		</div>
			    	<input type="radio" name="kandidat" id="<?php echo $radio;?>" value="<?php echo $value['id'];?>">
			    </div>
		    	<?php

		    }
		    ?>
		    <div class="cl"></div>
		    <div class="botton">
		    	<button class="btn btn-primary" type="submit">SUBMIT</button>
		    </div>
			<?php echo form_close(); ?>
		</div>
	</body>
</html>
<?php

/**
* Purchase Order
*/
class Purchase extends CI_Controller
{
	
	public function __construct()
	{
		
		# code...
		parent::__construct();
        $this->load->model('purchase_model');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }

	}

	public function index(){

		$data['title']			= "List Purchase Order";
		$data['main_content'] 	= 'admin/purchase/list';
		$data['purchase'] = $this->purchase_model->get_purchase();
		// echo "<pre>";
		// print_r($data['purchase']);
		// echo "</pre>";

		$this->load->view("includes/template",$data);

	}

	public function list_penawaran(){

		#no_penawaran
		#id_request
		#create purchase
		// $data = $this->purchase_model->get_penawaran();
		$data['title']			= "List Penawaran";
		$data['main_content'] 	= 'admin/penawaran/list';
		$data['penawaran'] 	= $this->purchase_model->get_penawaran();
		$this->load->view("includes/template",$data);

	}

	public function detail_penawaran(){

		$id = $this->uri->segment(4);
		$data['title']			= "Detail Penawaran";
		$data['main_content'] 	= 'admin/penawaran/detail';
		$data['penawaran'] 	= $this->purchase_model->get_penawaran($id);

		$list_barang = json_decode($data['penawaran'][0]['barang'], 1);

		$i = 1;
		foreach ($list_barang as $key => $value) {
			
			$detail_barang = $this->purchase_model->purchase_detail_barang($key);
			// echo "<pre>";
			// print_r($detail_barang);
			// echo "</pre>";

			$data['penawaran'][0]['barang_new'][$i]['id'] = $key;
			$data['penawaran'][0]['barang_new'][$i]['nama_barang'] = $detail_barang[0]['nama_barang'];
			$data['penawaran'][0]['barang_new'][$i]['harga_baru'] = $value;
			$data['penawaran'][0]['barang_new'][$i]['harga_lama'] = $detail_barang[0]['harga'];
			
			$i++;

		}


		$this->load->view("includes/template",$data);

	}

	public function add(){

		if ($this->input->server('REQUEST_METHOD') === 'POST'){

			echo "<pre>";
			print_r($_POST);
			echo "</pre>";

			$barang = explode(',', $this->input->post('barang'));
			$data = array();
			foreach ($barang as $value) {

				$harga = 'harga'.$value;
				$qty = 'qty'.$value;
				
				$data[$value]['harga'] = $this->input->post($harga);
				$data[$value]['qty'] = $this->input->post($qty);

			}

			$data_to_store = array(
                'no_penawaran' => $this->input->post('no_penawaran'),
                'barang_qty' => json_encode($data),
                'tanggal' => date('Y-m-d'),
            );
            
            $this->purchase_model->insert_purchase($data_to_store);
            redirect('admin/penawaran');

		}

		$data['title']			= "Add Purchase Order";
		$data['main_content'] 	= 'admin/purchase/add';
		// $this->load->view("includes/template",$data);

	}

	public function detail(){

		$id = $this->uri->segment(4);

		$data['title']			= "Detail Purchase Order";
		$data['main_content'] 	= 'admin/purchase/detail';
		$data['purchase'] = $this->purchase_model->get_purchase($id);

		$list_barang = json_decode($data['purchase'][0]['barang_qty'], 1);

		$i = 1;
		$all_harga = 0;
		foreach ($list_barang as $key => $value) {
			
			if ($value['qty'] != 0) {
				
				$detail_barang = $this->purchase_model->purchase_detail_barang($key);

				$data['purchase'][0]['barang'][$i]['id'] = $key;
				$data['purchase'][0]['barang'][$i]['nama_barang'] = $detail_barang[0]['nama_barang'];
				$data['purchase'][0]['barang'][$i]['Spesifikasi'] = $detail_barang[0]['Spesifikasi'];
				$data['purchase'][0]['barang'][$i]['harga'] = $value['harga'];
				$data['purchase'][0]['barang'][$i]['qty'] = $value['qty'];
				$data['purchase'][0]['barang'][$i]['total_harga'] = $value['harga']*$value['qty'];

				$all_harga += $value['harga']*$value['qty'];

			}
			$i++;


		}

		$data['all_harga'] = $all_harga;
		$this->load->view("includes/template",$data);

	}

}

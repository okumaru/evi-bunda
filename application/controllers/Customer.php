<?php

/**
* Controller Customer.php
*/
class Customer extends CI_Controller
{
	
	public function __construct()
	{

		parent::__construct();
		$this->load->model('customer_model');
        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }

	}

	public function index(){

		$data['title']			= "List Customer";
		$data['main_content'] 	= 'admin/customer/list';
		$data['customer'] = $this->customer_model->get_customer();

		$this->load->view("includes/template",$data);

	}

	public function detail(){

		$id = $this->uri->segment(4);
		
		$data['title']			= "Detail Customer";
		$data['main_content'] 	= 'admin/customer/detail';
		$data['customer'] = $this->customer_model->get_customer($id);

		$this->load->view("includes/template",$data);

	}

}
<?php

/**
* Class Barang
*/
class Barang extends CI_Controller
{
	
	public function __construct()
	{

		parent::__construct();
		$this->load->model('barang_model');
        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }

	}

	public function index(){

		$data['title']			= "List Barang";
		$data['main_content'] 	= 'admin/barang/list';
		$data['barang'] = $this->barang_model->get_barang();

		$this->load->view("includes/template",$data);

	}

    public function add(){

        $data['title']          = "Add Barang";
        $data['main_content']   = 'admin/barang/add';

        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 10000;
            
            // echo "<pre>";
            // print_r($_FILES['image']);
            // echo "</pre>";
            

            $this->load->library('upload', $config);
            $this->upload->do_upload('image');
            $this->upload->display_errors('<p>asdasd', '</p>');
            $img_name = $_FILES['image']['name'];

            $this->form_validation->set_rules('type', 'type', 'required');
            $this->form_validation->set_rules('nama_barang', 'nama_barang', 'required');
            $this->form_validation->set_rules('spesifikasi', 'spesifikasi', 'required');
            $this->form_validation->set_rules('harga', 'harga', 'required');
            if (! $img_name) {

                $this->form_validation->set_rules('image', 'image', 'required');

            }
            $this->form_validation->set_error_delimiters('
                <div class="alert alert-danger">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                      <i class="nc-icon nc-simple-remove"></i>
                    </button>
                    <span>
                    <b> Oh snap! - </b> change a few things up and try submitting again.</span>
                </div>
            ');
            
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                    'Type' => $this->input->post('type'),
                    'nama_barang' => $this->input->post('nama_barang'),
                    'Spesifikasi' => $this->input->post('spesifikasi'),
                    'harga' => $this->input->post('harga'),
                    'image' => $img_name,
                );
                if($this->barang_model->add_barang($data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/barang/add');

            }
        }

        $this->load->view("includes/template",$data);

    }

	public function update(){

        $id = $this->uri->segment(4);
		if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

        	$config['upload_path']          = 'uploads/';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 10000;
            $config['max_width']            = 1024;
            $config['max_height']           = 1024;

            $this->load->library('upload', $config);
            $this->upload->do_upload('image');
            $img_name = $_FILES['image']['name'];

            $this->form_validation->set_rules('type', 'type', 'required');
            $this->form_validation->set_rules('nama_barang', 'nama_barang', 'required');
            $this->form_validation->set_rules('spesifikasi', 'spesifikasi', 'required');
            $this->form_validation->set_rules('harga', 'harga', 'required');
            if (! $img_name) {

                $barang_detail = $this->barang_model->get_barang($id);
                $img_name = $barang_detail[0]['image'];
                // $this->form_validation->set_rules('image', 'image', 'required');

            }
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                    'Type' => $this->input->post('type'),
                    'nama_barang' => $this->input->post('nama_barang'),
                    'Spesifikasi' => $this->input->post('spesifikasi'),
                    'harga' => $this->input->post('harga'),
                    'image' => $img_name,
                );
                if($this->barang_model->update_barang($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/barang/update/'.$id);

            }

        }

		$data['title']			= "Edit Barang";
		$data['main_content'] 	= 'admin/barang/update';
		$data['barang'] = $this->barang_model->get_barang($id);

		$this->load->view("includes/template",$data);

	}

    public function delete(){

        $id = $this->uri->segment(4);
        $this->barang_model->delete_barang($id);
        redirect('admin/barang');

    }


}
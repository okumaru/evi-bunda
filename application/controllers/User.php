<?php

/**
* user
*/
class User extends CI_Controller
{
	
	public function index(){

		if($this->session->userdata('is_logged_in')){
			redirect('admin/customer');
	    }else{
	    	$this->load->view('admin/login/login');	
		}

	}

	function __encrip_password($password) {
        return md5($password);
    }

	function validate_credentials()
	{	

		$this->load->model('User_model');

		$user_name = $this->input->post('user_name');
		$password = $this->__encrip_password($this->input->post('password'));
		$is_valid = $this->User_model->validate($user_name, $password);
		
		if($is_valid)
		{
			$data = array(
				'user_name' => $user_name,
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);
			redirect('admin/customer');
		}
		else // incorrect username or password
		{
			$data['message_error'] = TRUE;
			$this->load->view('admin/login/login', $data);
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect('admin');
	}

}
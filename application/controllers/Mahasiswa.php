<?php

/**
* Mahasiswa
*/
class Mahasiswa extends CI_Controller
{
	
	public function __construct()
	{
		# code...
		parent::__construct();
        $this->load->model('mahasiswa_model');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
	}

	public function index()
	{
		$data['title']			= "List Mahasiswa";
		$data['main_content'] 	= 'admin/mahasiswa/list';
		$data['mhs'] = $this->mahasiswa_model->get_mhs();

		$this->load->view("includes/template",$data);
	}

	public function add()
	{

		if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            $this->form_validation->set_rules('nama', 'nama', 'required');
            $this->form_validation->set_rules('NIM', 'NIM', 'required');
            $this->form_validation->set_rules('pass', 'password', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            
            if ($this->form_validation->run())
            {
                $data_to_store = array(
                    'nama' => $this->input->post('nama'),
                    'NIM' => $this->input->post('NIM'),
                    'pass' => $this->input->post('pass'),
                );
                
                if($this->mahasiswa_model->store_mhs($data_to_store)){
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }

        $data['title']          = "Add Mahasiswa";
		$data['main_content']	= "admin/mahasiswa/add";

		$this->load->view("includes/template",$data);
	}

	public function update(){
		
        $nim = $this->uri->segment(4);
  
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            
            $this->form_validation->set_rules('nama', 'nama', 'required');
            $this->form_validation->set_rules('status', 'status', 'required');
            $this->form_validation->set_rules('pass', 'password', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                    'nama' => $this->input->post('nama'),
                    'status' => $this->input->post('status'),
                    'pass' => $this->input->post('pass'),
                );
                if($this->mahasiswa_model->update_mahasiswa($nim, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/mahasiswa/update/'.$nim);

            }

        }

        $data['title']			= "Edit Mahasiswa";
        $data['mhs']            = $this->mahasiswa_model->get_mahasiswa_by_id($nim);
        $data['main_content']   = 'admin/mahasiswa/edit';

        $this->load->view('includes/template', $data);

	}

    public function delete(){

        $id = $this->uri->segment(4);
        $this->mahasiswa_model->delete_mahasiswa($id);
        redirect('admin/mahasiswa');

    }

}
<?php

/**
* Request
*/
class Request extends CI_Controller
{
	
	public function __construct()
	{

		parent::__construct();
		$this->load->model('request_model');
        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }

	}

	public function index(){

		$data['title']			= "List Request";
		$data['main_content'] 	= 'admin/request/list';
		$data['request'] = $this->request_model->get_request();

		$this->load->view("includes/template",$data);

	}

	public function detail(){

		$id = $this->uri->segment(4);

		$data['title']			= "Detail Request";
		$data['main_content'] 	= 'admin/request/detail';
		$data['request'] = $this->request_model->request_detail($id);

		$this->load->view("includes/template",$data);

	}

	public function penawaran(){

		$id = $this->uri->segment(4);
		$data['title']			= "Buat Penawaran";
		$data['main_content'] 	= 'admin/request/penawaran';
		$data['request'] = $this->request_model->request_detail($id);
		
		$this->load->view("includes/template",$data);

	}

	public function view_penawaran(){

		if ($this->input->server('REQUEST_METHOD') === 'POST'){
			
			# insert to database
			# send email to client
			$this->form_validation->set_rules('no_penawaran', 'no_penawaran', 'required');
            $this->form_validation->set_rules('id_client', 'id_client', 'required');
            $this->form_validation->set_rules('id_request', 'id_request', 'required');
			$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

			if ($this->form_validation->run())
            {
    			
    			$arrBarang = array();
    			$barang = explode(',', $this->input->post('barang'));
    			$imgBarang = array();

    			foreach ($barang as $key => $value) {

    				$detail_barang = $this->request_model->get_barang($value);
    				$imgBarang[$value] = $detail_barang[0]['image'];

    				$fieldBarang = 'harga'.$value;
    				$arrBarang[$value] = $this->input->post($fieldBarang);

    			}

    			$jsonBarang = json_encode($arrBarang);
                $data_to_store = array(
                    'no_penawaran' 	=> $this->input->post('no_penawaran'),
                    'tanggal' 		=> date('Y-m-d'),
                    'id_customer' 	=> $this->input->post('id_client'),
                    'id_request' 	=> $this->input->post('id_request'),
                    'barang' 		=> $jsonBarang,
                );
                if($this->request_model->insert_penawaran($data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                // redirect('admin/request');

            }

		}

		$data['image']			= $imgBarang;
		$data['title']			= "Buat Penawaran";
		$data['main_content'] 	= 'admin/request/view_penawaran';
		
		$this->load->view("admin/request/view_penawaran",$data);

	}

}
<?php

/**
* Client
*/
class Client extends CI_Controller
{
	
	public function __construct()
	{
		# code...
		parent::__construct();
        $this->load->model('client_model');

	}

	public function index(){

		$data['main_content'] 	= 'client/home';
		$data['barang'] = $this->client_model->get_barang();

		$this->load->view("client/template",$data);

	}

	public function detail(){

		$id = $this->uri->segment(2);
		if ($id) {
			$data['main_content'] 	= 'client/detail';
			$data['barang'] = $this->client_model->get_barang($id);
			
			$this->load->view("client/template",$data);
		}else{

			redirect('index');

		}

	}

	public function add_product(){ # add to cart

		$id = $this->uri->segment(2);
		$arr = $this->session->userdata('products');
		if ($arr) {
			if (!in_array($id,$arr)) {
				array_push($arr, $id);
			}
		}else{
			$arr = array();
			array_push($arr, $id);
		}
		
		$data = array(
			'products' => $arr,
		);
		$this->session->set_userdata($data);

		$data['main_content'] 	= 'client/detail';
		$data['barang'] = $this->client_model->get_barang($id);
		$data['added'] = 1;
			
		$this->load->view("client/template",$data);

	}

	public function remove_product(){ # remove product from cart

		$id = $this->uri->segment(2);
		$arr = $this->session->userdata('products');
		$index = array_search($id,$arr);
		unset($arr[$index]);
		$this->session->unset_userdata('products');
		$this->session->set_userdata(
			array('products' => $arr)
		);
		redirect('cart');

	}

	public function cart(){

		$arr = $this->session->userdata('products');
		if ($arr) {
			
			$data = array();
			foreach ($arr as $key => $value) {
				$barang[$value] = $this->client_model->get_barang($value);
			}
			$data['barang'] = $barang;

		}

		$data['main_content'] 	= 'client/cart';
		$this->load->view("client/template",$data);

	}

	public function penawaran(){

		// echo "asd";
		# 1. save hasil cart ke database
		#	- data product yang di tambahkan
		#	- user yang di tambahkan
		# 2. melakukan kirim email ke client
		#	- sesuai dengan product yg ada di cart
		if ($this->input->server('REQUEST_METHOD') === 'POST'){

            $this->form_validation->set_rules('products', 'products', 'required');
            $this->form_validation->set_rules('nama', 'nama', 'required');
            $this->form_validation->set_rules('alamat', 'alamat', 'required');
            $this->form_validation->set_rules('email', 'email', 'required');
            $this->form_validation->set_rules('tlpn', 'tlpn', 'required');
            $this->form_validation->set_rules('pesan', 'pesan', 'required');
            $this->form_validation->set_error_delimiters('<div>asd', '</div>');

            $data_to_store = array();
            if ($this->form_validation->run())
            {
    
    			$data['error'] = 0;
                $data_to_store['customer'] = array(
                    'nama_customer' => $this->input->post('nama'),
                    'alamat' => $this->input->post('alamat'),
                    'email' => $this->input->post('email'),
                    'tlp' => $this->input->post('tlpn'),
                );
                $data_to_store['request'] = array(
                	'id_barang' => $this->input->post('products'),
                	'pesan' => $this->input->post('pesan'),
                );

                // $this->client_model->insert_penawaran($data_to_store);
                // echo $this->client_model->insert_penawaran($data_to_store);
                if($this->client_model->insert_penawaran($data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'inserted');
                    // echo "asdasdasd";
                }else{
                    $this->session->set_flashdata('flash_message', 'not_inserted');
                    // echo "qweqweqwe";
                }
                $this->session->unset_userdata('products');

                #========
                # send email client - HOLD
                #========

            }else{

            	// $data['error'] = 1;
            	echo "<script>alert('Lengkapi Fill terlebih dahulu');</script>";
            	$data['error'] = 1;

            }

		}

		$data['main_content'] 	= 'client/penawaran';
		$this->load->view("client/template",$data);

	}

	public function purchase(){

		$id = $this->uri->segment(2);
		if (empty($id)) {

			$data['step'] 	= 0;

		}else{

			$data['step'] 	= 1;
			$result_request = $this->client_model->get_second_penawaran($id);
			$barang = json_decode($result_request[0]['barang'], 1);
			foreach ($barang as $key => $value) {
				
				$barang = $this->client_model->get_barang($key);
				$data_barang[$key]['id_barang'] = $barang[0]['id_barang'];
				$data_barang[$key]['nama_barang'] = $barang[0]['nama_barang'];
				$data_barang[$key]['harga'] = $value;

			}

			$data['no_penawaran'] = $result_request[0]['no_penawaran'];
			$data['barang'] 	= $data_barang;
			$data['id_customer'] 	= $result_request[0]['id_customer'];

		}

		if ($this->input->server('REQUEST_METHOD') === 'POST'){
			
			$check_id = $this->input->post('one');
			$no_penawaran = $this->input->post('no_penawaran');
			if (!empty($check_id)) {
				# code...

				$get_purchase = $this->client_model->get_purchase($no_penawaran);
				if ($get_purchase) {
					
                	$this->session->set_flashdata('flash_message', 'updated');

				}else{

					$result_request = $this->client_model->get_penawaran($no_penawaran);
					if ($result_request) {
						redirect('purchase/'.$result_request[0]['id_request']);
					}else{
						$this->session->set_flashdata('flash_message', 'empty');
					}

				}

			}else{

				$check_id = $this->input->post('two');
				$id_customer = $this->input->post('client');

				$result_request = $this->client_model->get_penawaran($no_penawaran);
				$barang = json_decode($result_request[0]['barang'], 1);
				$harga_total = 0;
				$barang_qty = array();
				foreach ($barang as $key => $value) {
					
					#
					if (empty($this->input->post('qty'.$key))) {
						$qty = 0;
					}else{
						$qty = $this->input->post('qty'.$key);
					}
					$barang_qty[$key]['harga'] = $value;
					$barang_qty[$key]['qty'] = $qty;
					$total = $value*$qty;
					$harga_total += $total;

					// $this->input->get_barang

				}

				$barang = json_encode($barang_qty);
				$data_to_store = array(
                    'no_penawaran' => $no_penawaran,
                    'barang_qty' => $barang,
                    'tanggal' => date('Y-m-d'),
                );

                $data['customer'] = $this->client_model->id_customer($id_customer);
                $data['no_penawaran'] = $no_penawaran;
                $data['barang_qty'] = $barang_qty;
                $data['harga_total'] = $harga_total;

                $this->client_model->insert_purchase($data_to_store);
                $this->session->set_flashdata('flash_message', 'updated');

			}

		}
		$data['main_content'] 	= 'client/purchase';
		$this->load->view("client/template",$data);

	}

}
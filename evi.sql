-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 03 Jan 2018 pada 14.45
-- Versi Server: 10.1.8-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `evi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(10) NOT NULL,
  `Type` varchar(10) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `Spesifikasi` text NOT NULL,
  `harga` int(20) NOT NULL,
  `image` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id_barang`, `Type`, `nama_barang`, `Spesifikasi`, `harga`, `image`) VALUES
(28, 'DO-01', 'Mesin Pengaduk Dodol 10kg', 'Kapasitas		: 10Kg /proses\r\nDimensi Bejana	: Dalam : Ø 35 x 40 cm \r\n			  Luar    : Ø 43 x 45 cm\r\nSistem Pemanasan	: Tangki Double Jacket \r\nSistem Pengadukan	: Pengaduk Kayu Horizontal-Vertikal\r\nSistem Tuang Bejana	: Di jungkir 90? (Menggunakan Slot)\r\nBahan			: Frame     		: Stainless Steel, 2mm\r\n   	  Tabung Silinder  	: Stainless Steel 304, 2mm \r\nDimensi (pxlxt)cm	: 60 x 50 x 170 cm (Menyesuiakan)\r\nDaya Listrik Max	: 350 W / 220 VA\r\nBahan Bakar Panas	: Burner LPG\r\nPutaran RPM		: 30 - 40 RPM\r\nTransmisi		: Gear Box, Pulley, V Belt\r\n', 8750000, 'pengaduk_dodol.jpg'),
(29, 'Sangrai-05', 'Mesin Sangrai Kopi 5kg', 'Kapasitas (Kg)	: 5 Kg/proses \r\nDimensi (p x l x t)	: 100x50x90 cm\r\nBahan	: Stainless Steel\r\nDimensi Tabung	: Diameter Ø30 cm Panjang 45 cm\r\nTebal Plat SS	: 2 mm (Tabung)\r\n	  1 mm (Body)\r\nPenggerakMotorDinamo	: 250 Watt\r\nKontrol Suhu	: Terkontrol Otomatis (Thermokontrol & Selenoid)        \r\nTransmisi 	: Gearbox 1:50 tipe60    \r\nPemanas	: Burner LPG\r\nKelengkapan	: Kompor COR, Selang, Regulator.\r\n', 8500000, 'sangrai_kopi-3.jpg'),
(31, 'VC-05', 'Vacuum Frying 5 kg', 'Kapasitas (Kg)			: 5 - 6kg Bahan Baku             \r\nType				: Horisontal PV 1\r\nDimensi Tabung   \r\nPenggorengan (Øxp)cm   	: 45x80cm\r\nDimensi Bak Air                 	: 182x122x65cm\r\nDimensi Total (pxlxt)cm    	: 182x125x120cm\r\nBahan Bakar			: LPG   \r\nBurner				: kompor low pressure Isi 2-burner                \r\nVolumeMinyakGoreng (lt)  	: 55 lt                      \r\n  Penggerak Vakum		: Sistem Water Jet Tunggal / Single Jet\r\nKebutuhan Daya Listrik      	: 750 Watt          \r\nKontrol Suhu			: Digital\r\nKelengkapan			: Spiner (Peniris), Hand Sealer 30cm\r\n', 14500000, 'vacuum_frying.jpg'),
(32, 'Spin-1', 'Mesin Peniris Minyak 1,5kg', 'Fungsi : meniriskan minyak pada makanan ( kripik dsb )\r\nKapasitas	: 1,5 Kg Bahan Baku\r\nDimensi Keranjang	: Ø 24 cm, tinggi = 19 cm\r\nDimensi Tabung Luar	: Ø 28 cm, tinggi = 36 cm\r\nBahan : Frame	: Pipa Besi Kotak\r\n              Silinder	: Stainless Steel\r\n              Keranjang Bahan	: Vorporasi Stainless steel\r\nDaya	: 250 Watt,220V\r\nRPM	: 500 - 600 rpm\r\n', 1700000, 'spinner.jpg'),
(33, 'Roaster-3', 'Mesin Roaster Kopi 3kg', 'Kapasitas:			: 3 Kg / Batch\r\nDimensi (p x l x t)            		 : 110 x 40 x 80 Cm\r\nDimensi Tabung                		: Diameter Ø30 cm Panjang 35 cm\r\nMaterial				: \r\nTabung roaster		: Stainless Steel 3 Mm\r\nCover			: Stainless Steel & Mild Steel 1 Mm\r\nBak Cooler		: Stainless Steel\r\nRangka 			: Pipa Mild Steel \r\nKontrol Suhu                  		 : Terkontrol Otomatis (Thermokontrol & Selenoid)        \r\nBlower 				: 0,3 A\r\nPenggerak Sangrai		: Dinamo Motor 1/4 Hp / 750 Watt\r\nPenggerak Cooler		: Dinamo Motor \r\nPemanas 			: Burner LPG\r\nTransmisi 			: Gearbox & pulley\r\nKelengkapan                  		 : Kompor COR, Selang, Regulator\r\n', 16000000, 'roaster.jpg'),
(35, 'PRJ-V', 'Mesin Perajang Vertikal', 'Kapasitas		: 75 kg/jam\r\nBahan\r\n•	Disk Pisau	: Besi Chrom	\r\n•	Pisau		: Stainless Steel\r\n•	Frame		: Rangka Besi\r\n•	Cover		: Stainless Steel\r\n•	Hopper		: Stainless Steel\r\nDiameter Disc	: Ø 35 cm\r\nJumlah Pisau		: 2 - 3 Buah\r\nPanjang Pisau		: ± 10 - 12 cm \r\nPosisi Disk		: Vertikal\r\nPenggerak		: Elektro Motor 1/4 PK\r\n', 4500000, 'perajang_umbi-3.jpg'),
(36, 'KR-PRJ', 'Mesin Perajang Kerupuk Otomatis', 'Kapasitas	: 200 kg/jam\r\nSistem	: Feeder / Pengumpan\r\nBahan\r\n•	Disk Pisau	: Besi Chrom	\r\n•	Pisau	: Baja\r\n•	Frame	: Rangka Besi\r\nDiameter Disc		: Ø 30 cm\r\nJumlah Pisau	: 2 - 3 Buah\r\nPanjang Pisau	: ± 10 cm \r\nPosisi Disk	: Vertikal\r\nPenggerak	: Elektro Motor 500 Watt\r\n', 14000000, 'perajang_kerpuk.jpg'),
(37, 'Kertas-100', 'Mesin Penghancur Kertas 100kg', 'Kapasitas             	: 150-200 kg/jam\r\nPenggerak			  : Elektro Motor 2 PK\r\nDimensi                   	: 65 x 55 x 90 cm\r\nBahan                        	: body : plat mild steel 2mm\r\n		   Frame besi siku L\r\nModel Pisau		: Pisau Roller\r\nPanjang Pisau		: Ø5 x 22cm\r\nHasil Cacahan		: 8 – 10mm\r\n', 15000000, 'penghancur_kertas-3.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_cookies`
--

CREATE TABLE `ci_cookies` (
  `id` int(11) NOT NULL,
  `cookie_id` varchar(255) DEFAULT NULL,
  `netid` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `orig_page_requested` varchar(120) DEFAULT NULL,
  `php_session_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(50) NOT NULL,
  `ip_address` varchar(10) NOT NULL,
  `user_agent` varchar(100) NOT NULL,
  `last_activity` varchar(50) NOT NULL,
  `user_data` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('ceeae5ee225d6ffe5070841716c6e32e', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506910947', ''),
('0f1f35b68da97f81a639cee7a807a168', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506913454', ''),
('171ffa825919ceeea317dd7c92c411c6', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506914153', ''),
('7e4aa68cb3ef57594d35d2f65800115b', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506914943', ''),
('4f470f84ff66d3fb00d0c6b97c3b7c32', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506914977', ''),
('95f3f8025902ec3da806eda479497812', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506914993', ''),
('1d5d63e710995072d629b3c6fc348a00', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506915002', ''),
('ebe9e2b2293798c30248aca029b18ede', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506915032', 'a:3:{s:9:"user_data";s:0:"";s:9:"user_name";s:7:"o'),
('109066c9bd0990c95ccdccf30bb2cd6b', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506915103', ''),
('80e76a41583dd25d16c97053cf7c241b', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506915109', 'a:3:{s:9:"user_data";s:0:"";s:9:"user_name";s:7:"o'),
('3dbc8445d616fc8455d83c8015ffc1e7', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506915109', ''),
('16897d9b174ab5ad8854b2e1fce8af12', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506915109', ''),
('d34a695c881d300276d5b2cb0cd0595c', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506915982', 'a:3:{s:9:"user_data";s:0:"";s:9:"user_name";s:7:"o'),
('3d1b0e4439d0e6ec0a596a1ad7d79ddc', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506915983', ''),
('a097dfc13dd1851ad231332aaa500496', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506915984', ''),
('073596216f7d2dc7b6fac5c52a47ff4b', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916148', 'a:3:{s:9:"user_data";s:0:"";s:9:"user_name";s:7:"o'),
('9b81361460f9c9f1963462e248d778f1', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916149', ''),
('4c3e2bea9dd161bef397a5a1b1f4d028', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916149', ''),
('e73d5475792bea8e9c4ce63a58ea8c64', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916174', ''),
('8cf2cfb4bca2e415b7e73e9630df4e45', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916179', 'a:3:{s:9:"user_data";s:0:"";s:9:"user_name";s:7:"o'),
('1b47a13e3e7e2c16f52d5d2b7a4a181f', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916179', ''),
('cc4955eb0c487cc42062835786128e4a', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916179', ''),
('f3e45a69081b40587346fe0455d496c5', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916378', ''),
('40b99dd7aaf383eea1c1c423517a58e7', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916690', ''),
('16ed52372a2360ca23e93ff2ce9a970b', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916782', ''),
('99431e4b1930f6d50c42ea894b74919d', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916846', ''),
('49a5338fd2af074c47774a4a6afa772d', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916859', ''),
('f098dd909b427a834aa1965e20c7b430', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506916973', ''),
('48e623f15d23e8177fb853aa2f1a8f31', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917029', ''),
('9687076dca0c28d6c8c957779607468b', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917065', ''),
('39e2bde0495c0fdf3fa86b3cf5ffd274', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917420', ''),
('e251c7b40c0dfc2f4b53342e9c1ef7fa', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917530', ''),
('b156252504f03ec38c179a8b46989877', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917677', ''),
('ed2acf4eb7cdba38a2f0490fde860794', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917716', ''),
('479bb7bb24ddd3a2145aea6e9b34cc90', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917726', ''),
('47748dda9c8139b4a6e8e8c9f01be862', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917732', ''),
('d1cf9969027a1ad527811e1f7cdf83ea', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917900', ''),
('d7df23b21cdce1c6adb086b25c7fb110', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917914', ''),
('f3b587606640d4c2787fafc04ec920e0', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917931', ''),
('a50da2a343402c93a06e6334841da5ef', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917967', ''),
('d67ee91349bb2609a506c84385030eb7', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917972', ''),
('6ea34091ee6528b4f92f5b5ca19e234d', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506917983', ''),
('8563f283d1ccdfc23dc9dcaa1f5aec9d', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506918042', ''),
('fde846194ebab2efc38e7aed49c88a0a', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506918044', ''),
('91ccd24373058007da4b4c965aab5c66', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506918045', ''),
('6c3490aef1cc82f8bfabc599e749d526', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506918046', ''),
('a52b0969c6dbf45020fce958327cc4b8', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506918367', ''),
('548c383b7fad5e21bec8f9f17a9ff1e7', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506918468', ''),
('62fc16f879dba0871707d1c06b164ee7', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506918634', ''),
('8802fcad19b0b21e4fdd64cb5f392947', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506918635', ''),
('7e5cd6f265f43ddcf38ae737f7937480', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506918733', ''),
('d5377db7c87a0913d592b860de0a02d3', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506919301', ''),
('98d3c62c55df72f75fd75c79365daa3a', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506919364', ''),
('412daf92daddd29b4224cd9f6e3d7c89', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506919419', ''),
('781e26561221b95c0d47fec40ddaedaa', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506920932', ''),
('b0282d0a381a2f408c9285b7a3c99a86', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506921626', ''),
('6ac1c86294536d9044dc670adf2ca189', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506921638', ''),
('4b533cd880ffdbab258e20dfd51bfcc6', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506921698', ''),
('c76d3bb464321984c27b233ffbf774f5', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506921938', ''),
('aa76d620bf6501fb831d824a10ce4e41', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506921966', ''),
('4c7102fa858a78485724047729538bab', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922067', ''),
('09b08cd316fbb008e1432ab97955d02e', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922412', ''),
('a4d20eca7498ba8fadc133fc94311510', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922414', ''),
('69209f7f5e14ad232986a428a2035fd4', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922440', ''),
('61fb5852556e4468242bbccc695da691', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922520', ''),
('4d84433c1e8cfb5212bee05a506fa997', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922527', ''),
('95e9aae6d4dd965a666d13337ba27d48', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922631', ''),
('d1e36a34ae6633cdc5d1907dbb366c98', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922660', ''),
('515b88a4b85fbcd79455aefe663f882b', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922674', ''),
('570771165dea530b724802bb187841c6', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922911', ''),
('335d6af2ab01fba303df6d893de49afa', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922932', ''),
('0f1fb3ab9341f19ca9cc2aaf4039ac31', '::1', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537', '1506922935', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(10) NOT NULL,
  `nama_customer` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tlp` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `alamat`, `email`, `tlp`) VALUES
(1001, 'Bambang', 'jl. candi mendut mlg', 'asdqwezxc@gmail.com', '80987976'),
(1002, 'Bambang', 'jl. candi mendut mlg', '', '80987976'),
(1019, 'percobaan 1', 'percobaan 1', 'dika@dika.com', '082331331331');

-- --------------------------------------------------------

--
-- Struktur dari tabel `invoice`
--

CREATE TABLE `invoice` (
  `id_invoice` int(10) NOT NULL,
  `id_purchase` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `harga_unit` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `dp` int(11) NOT NULL,
  `kekurangan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `membership`
--

CREATE TABLE `membership` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email_addres` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `pass_word` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `membership`
--

INSERT INTO `membership` (`id`, `first_name`, `last_name`, `email_addres`, `user_name`, `pass_word`) VALUES
(1, 'dika', 'test', 'dika.martha.test@gmail.com', 'okumaru', '45f1679d96cb76ae480dab80a0948979');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penawaran`
--

CREATE TABLE `penawaran` (
  `no_penawaran` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_request` int(11) NOT NULL,
  `barang` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penawaran`
--

INSERT INTO `penawaran` (`no_penawaran`, `tanggal`, `id_customer`, `id_request`, `barang`) VALUES
('014/SP/GM/XII/17', '2018-01-03', 1019, 14, '{"36":"14000000","28":"8750000"}');

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchase`
--

CREATE TABLE `purchase` (
  `id` int(11) NOT NULL,
  `no_penawaran` int(10) NOT NULL,
  `barang_qty` varchar(100) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `purchase`
--

INSERT INTO `purchase` (`id`, `no_penawaran`, `barang_qty`, `tanggal`) VALUES
(2, 1, '{"28":"3","33":"2"}', '2017-12-19'),
(3, 1, '{"28":"1","33":"1"}', '2017-12-19'),
(4, 1, '{"28":0,"33":0}', '2017-12-19'),
(5, 1, '{"28":0,"33":0}', '2017-12-19'),
(6, 1, '{"28":0,"33":0}', '2017-12-19'),
(7, 1, '{"28":0,"33":0}', '2017-12-19'),
(8, 1, '{"28":0,"33":0}', '2017-12-19'),
(9, 1, '{"28":0,"33":0}', '2017-12-19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `request`
--

CREATE TABLE `request` (
  `kode_offer` int(20) NOT NULL,
  `tanggal` date NOT NULL,
  `id_customer` int(11) NOT NULL,
  `barang` varchar(30) NOT NULL,
  `pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `request`
--

INSERT INTO `request` (`kode_offer`, `tanggal`, `id_customer`, `barang`, `pesan`) VALUES
(13, '2017-12-28', 1019, '31', 'percobaan 1'),
(14, '2017-12-28', 1019, '36,28', 'percobaan 1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(5) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`) VALUES
(1, 'Evy', '3828'),
(2, 'sary', '123'),
(3, 'agen', 'mesin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id_invoice`),
  ADD KEY `id_purchase` (`id_purchase`);

--
-- Indexes for table `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penawaran`
--
ALTER TABLE `penawaran`
  ADD PRIMARY KEY (`no_penawaran`),
  ADD KEY `id_request` (`id_request`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_offer` (`no_penawaran`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`kode_offer`),
  ADD KEY `id_barang` (`barang`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1020;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id_invoice` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `membership`
--
ALTER TABLE `membership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `kode_offer` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`id_purchase`) REFERENCES `purchase` (`id`);

--
-- Ketidakleluasaan untuk tabel `penawaran`
--
ALTER TABLE `penawaran`
  ADD CONSTRAINT `penawaran_ibfk_1` FOREIGN KEY (`id_request`) REFERENCES `request` (`kode_offer`);

--
-- Ketidakleluasaan untuk tabel `request`
--
ALTER TABLE `request`
  ADD CONSTRAINT `request_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

require_once('PHPMailer/PHPMailerAutoload.php');

function smtp_mail($to, $subject, $message, $from_name, $from, $cc, $bcc, $debug=false, $attachment=array()) {
  $mail = new PHPMailer;
  $mail->SMTPDebug = $debug; // Ubah menjadi true jika ingin menampilkan sistem debug SMTP Mailer
  $mail->isSMTP();
  
  // Hapus Semua Tujuan, CC dan BCC
  $mail->ClearAddresses();  
  $mail->ClearCCs();
  $mail->ClearBCCs();

  /* -------------------------- Konfigurasi Dasar SMTP ---------------------------------- */
  
  $mail->SMTPAuth 	= true;                        				
  $mail->Host 		  = 'smtp.gmail.com';  // Masukkan Server SMTP
  $mail->Port 		  = 587;                                      // Masukkan Port SMTP
  $mail->SMTPSecure = 'tls';                                    // Masukkan Pilihan Enkripsi ( `tls` atau `ssl` )
  $mail->Username 	= 'dika@jagoanhosting.com';                // Masukkan Email yang digunakan selama proses pengiriman email via SMTP
  $mail->Password 	= 'opp00opp';        						          // Masukkan Password dari Email tsb
  $default_email_from       = 'dika@jagoanhosting.com';        // Masukkan default from pada email
  $default_email_from_name  = 'Informasi Garuda';           // Masukkan default nama dari from pada email
  
  /* -------------------------- Konfigurasi Dasar SMTP ---------------------------------- */
  
  if(empty($from)) $mail->From = $default_email_from;
  else $mail->From = $from;

  if(empty($from_name)) $mail->FromName = $default_email_from_name;
  else $mail->FromName = $from_name;
  
  // Set penerima email
  if(is_array($to)) {
    foreach($to as $k => $v) {
      $mail->addAddress($v);
    }
  } else {
    $mail->addAddress($to);
  }
  
  // Set email CC ( optional )
  if(!empty($cc)) {
    if(is_array($cc)) {
      foreach($cc as $k => $v) {
        $mail->addCC($v);
      }
    } else {
      $mail->addCC($cc);
    }
  }
  
  // Set email BCC ( optional )
  if(!empty($bcc)) {
    if(is_array($bcc)) {
      foreach($bcc as $k => $v) {
        $mail->addBCC($v);
      }
    } else {
      $mail->addBCC($bcc);
    }
  }
  
  // Set isi dari email
  $mail->isHTML(true);
  $mail->Subject 	= $subject;
  $mail->Body = $message;

  if (!empty($attachment)) {
    foreach ($attachment as $key => $value ) {
      $mail->addAttachment($value);
    }
  }
  
  // for ($ct = 0; $ct < count($_FILES['userfile']['tmp_name']); $ct++) {
  //   $uploadfile = tempnam(sys_get_temp_dir(), hash('sha256', $_FILES['userfile']['name'][$ct]));
  //   $filename = $_FILES['userfile']['name'][$ct];
  //   if (move_uploaded_file($_FILES['userfile']['tmp_name'][$ct], $uploadfile)) {
  //     $mail->addAttachment($uploadfile, $filename);
  //   } else {
  //     $msg .= 'Failed to move file to ' . $uploadfile;
  //   }
  // }

  $mail->AltBody	= $message;
  if(!$mail->send())
    return 1;
  else
    return 0;
}

?>